﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Inventory))]
public class InventoryEditor : Editor {

    private bool ShowItems;

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        /*
        Inventory invTarget = (Inventory)target;
        if (invTarget.Items == null) {
            invTarget.Items = new Item[0];
        }
        ShowItems = EditorGUILayout.Foldout(ShowItems, "Items");
        if (ShowItems) {
            EditorGUI.indentLevel++;
            for (int i = 0; i < invTarget.Items.Length; i++) {
                SerializedProperty nameProperty = serializedObject.FindProperty("Name");
                SerializedProperty iconProperty = serializedObject.FindProperty("Icon");
                SerializedProperty idProperty = serializedObject.FindProperty("Id");
                SerializedProperty ouncesProperty = serializedObject.FindProperty("Ounces");
                SerializedProperty valueProperty = serializedObject.FindProperty("Value");
                SerializedProperty valueRemainingProperty = serializedObject.FindProperty("ValueRemaining");
                EditorGUILayout.PropertyField(nameProperty);
                EditorGUILayout.PropertyField(iconProperty);
                EditorGUILayout.PropertyField(idProperty);
                EditorGUILayout.PropertyField(ouncesProperty);
                EditorGUILayout.PropertyField(valueProperty);
                EditorGUILayout.PropertyField(valueRemainingProperty);
            }
            EditorGUI.indentLevel--;
        }       
        */
        serializedObject.ApplyModifiedProperties();
    }
}
