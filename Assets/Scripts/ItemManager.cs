﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;

public class ItemManager : MonoBehaviour {
    private static ItemManager _I;
    public static ItemManager I {
        get {
            if (_I == null) {
                _I = FindObjectOfType<ItemManager>();
            }
            return _I;
        }
    }

    [Header("Item Data")]
    public Item[] Items; // instantiated in CVSReader

    private int _numOfItems;
    public int NumOfItems { get { return _numOfItems; } set { _numOfItems = NumOfItems; } }

    public Item GetItem(int itemId) {
        return Items[itemId];
    }
}
