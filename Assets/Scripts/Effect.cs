﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Effect {
    public int HungerRecover;
    public int ThirstRecover;
    public int HealthRecover;
    public int EnergyRecover;
    public int MeatRecover;
    public int DairyRecover;
    public int GrainRecover;
    public int VegetableRecover;
    public int FruitRecover;

    public Effect(int hungerRecover, int thirstRecover, int healthRecover,
                  int energyRecover, int meatRecover, int dairyRecover,
                  int grainRecover, int vegetableRecover, int fruitRecover) {
        HungerRecover = hungerRecover;
        ThirstRecover = thirstRecover;
        HealthRecover = healthRecover;
        EnergyRecover = energyRecover;
        MeatRecover = meatRecover;
        DairyRecover = dairyRecover;
        GrainRecover = grainRecover;
        VegetableRecover = vegetableRecover;
        FruitRecover = fruitRecover;
    }
}
