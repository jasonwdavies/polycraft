﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// My super-class for items to avoid inheritence and allow for easy creation of items from a csv file
/// </summary>
[System.Serializable]
public class Item {

    public enum KeyIngredient { None, Wood, Stone, Copper }

    [Header("Base")]
    public string Name;
    public Sprite Icon;
    public int Id;
    public int Quantity;
    public float LifeSpan;
    public float LifeSpanRemaining;
    public float TimeToUse;
    public KeyIngredient IsKeyIngredientType;

    [Header("Consumable")]
    public bool IsConsumable;
    public Effect ConsumeEffect;
    public int MaxConsumableStack;
    public float TimeCreated;

    [Header("Seed")]
    public bool IsSeed;
    public int GrowsIntoID;

    [Header("Weapon")]
    public bool IsWeapon;
    public int SlashingDamage;
    public int PiercingDamage;
    public int CrushingDamage;

    [Header("Armor")]
    public bool IsArmor;
    public int ArmorAmount;

    [Header("Container")]
    public bool IsContainer;
    public bool CanCarryLava;
    public bool CanCarryLiquidMetal;
    public int ContainerAmount;

    [Header("Block")]
    public Block BlockInfo;
     
    // Used to create the items in ItemManager
    public Item(string name, Sprite icon, int id, int quantity, float lifeSpan,
                float lifeSpanRemaining, float timeToUse, KeyIngredient isKeyIngredientType,
                bool isConsumable, Effect consumeEffect, int maxConsumableStack, 
                float timeCreated, bool isSeed, int growsIntoID, bool isWeapon,
                int slashingDamage, int piercingDamage, int crushingDamage,
                bool isArmor, int armorAmount, bool isContainer, bool canCarryLava,
                bool canCarryLiquidMetal, int containerAmount, Block blockInfo) {
        Name = name;
        Icon = icon;
        Id = id;
        Quantity = quantity;
        LifeSpan = lifeSpan;
        LifeSpanRemaining = lifeSpanRemaining;
        TimeToUse = timeToUse;
        IsKeyIngredientType = isKeyIngredientType;

        IsConsumable = isConsumable;
        if (IsConsumable) {
            ConsumeEffect = consumeEffect;
            TimeCreated = timeCreated;
            MaxConsumableStack = maxConsumableStack;
        }

        IsSeed = isSeed;
        if (IsSeed) {
            GrowsIntoID = growsIntoID;
        }

        IsWeapon = isWeapon;
        if (IsWeapon) {
            SlashingDamage = slashingDamage;
            PiercingDamage = piercingDamage;
            CrushingDamage = crushingDamage;
        }

        IsArmor = isArmor;
        if (IsArmor) {
            ArmorAmount = armorAmount;
        }

        IsContainer = isContainer;
        if (IsContainer) {
            CanCarryLava = canCarryLava;
            CanCarryLiquidMetal = canCarryLiquidMetal;
            ContainerAmount = containerAmount;
        }

        BlockInfo = blockInfo;
    }

    // Used to create items during runtime
    public Item(Item item) {
        Name = item.Name;
        Icon = item.Icon;
        Id = item.Id;
        Quantity = item.Quantity;
        LifeSpan = item.LifeSpan;
        LifeSpanRemaining = item.LifeSpanRemaining;
        TimeToUse = item.TimeToUse;
        IsKeyIngredientType = item.IsKeyIngredientType;

        IsConsumable = item.IsConsumable;
        if (IsConsumable) {
            ConsumeEffect = item.ConsumeEffect;
            TimeCreated = Clock.I.TotalMinutes;
            MaxConsumableStack = item.MaxConsumableStack;
        }

        IsSeed = item.IsSeed;
        if (IsSeed) {
            GrowsIntoID = item.GrowsIntoID;
        }

        IsWeapon = item.IsWeapon;
        if (IsWeapon) {
            SlashingDamage = item.SlashingDamage;
            PiercingDamage = item.PiercingDamage;
            CrushingDamage = item.CrushingDamage;
        }

        IsArmor = item.IsArmor;
        if (IsArmor) {
            ArmorAmount = item.ArmorAmount;
        }

        IsContainer = item.IsContainer;
        if (IsContainer) {
            CanCarryLava = item.CanCarryLava;
            CanCarryLiquidMetal = item.CanCarryLiquidMetal;
            ContainerAmount = item.ContainerAmount;
        }

        BlockInfo = item.BlockInfo;
    }

    #region Base Functions

    public bool IsOfCategory(KeyIngredient isKeyIngredientType) {
        return IsKeyIngredientType == isKeyIngredientType;
    }

    public bool Equals(Item item) {
        return Name.Equals(item.Name);
    }

    #endregion

    #region Consumable Functions

    public void Decay() {
        LifeSpanRemaining = LifeSpan - Mathf.Pow(2, 0.1f * ((float)Clock.I.TotalMinutes - TimeCreated));
    }

    public bool IsExpired() {
        return LifeSpanRemaining <= 0;
    }

    #endregion
}
