﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bush : Tree {

    public int PlantID;
    public int SeedID;
    public TilledDirt Dirt;
    public float MinTempToGrow;
    public float MaxTempToGrow;
    public float SeedDropChance;
    public int PlantMinCount;
    public int PlantMaxCount;
    public int PlantCount;
    public int SeedCount;

    private new void Start() {
        base.Start(); // handles growing of the bush
    }

    private new void Update() {
        if (_mature && PlantCount == 0 && (Clock.I.TotalMinutes / (60 * 24)) > _lastDayChecked) {
            if (MinTempToGrow <= GlobalInfo.I.Temperature && MaxTempToGrow >= GlobalInfo.I.Temperature) {
                GrowPlant();
            }
            _lastDayChecked = Clock.I.TotalMinutes / (60 * 24);
        }
        base.Update();
    }

    private void GrowPlant() {
        PlantCount = Random.Range(PlantMinCount, PlantMaxCount);
        SeedCount = Random.Range(PlantMinCount, PlantMaxCount);
    }

    public void HarvestPlant() {
        if (PlantCount > 0) {
            Item plant = ItemManager.I.GetItem(PlantID);
            ItemContainer itemContainer = Instantiate(GlobalInfo.I.ItemContainer).GetComponent<ItemContainer>();
            itemContainer.gameObject.transform.position = transform.position + new Vector3(-1, 2, -1);
            itemContainer.PickupItem = plant;
            itemContainer.PickupItem.Quantity = PlantCount;

            if (Random.value >= (1 - SeedDropChance)) {
                Item seed = ItemManager.I.GetItem(SeedID);
                ItemContainer seedContainer = Instantiate(GlobalInfo.I.ItemContainer).GetComponent<ItemContainer>();
                seedContainer.gameObject.transform.position = transform.position + new Vector3(-1, 2, 1);
                seedContainer.PickupItem = seed;
                seedContainer.PickupItem.Quantity = SeedCount;
            }
            Destroy(gameObject);
        }
    }
}
