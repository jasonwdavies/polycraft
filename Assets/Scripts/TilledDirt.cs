﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilledDirt : MonoBehaviour {

    public bool Occupied;

    public void TryPlantSeed(Item seed) {
        if (!Occupied) {
            PlantSeed(seed);
        }
    } 

    public void PlantSeed(Item seed) {
        Item plant = ItemManager.I.GetItem(seed.GrowsIntoID);
        GameObject bush;

        switch (plant.Name) {
            case "Blackberry":
                bush = Instantiate(GlobalInfo.I.BlackberryBush);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "Blueberry":
                bush = Instantiate(GlobalInfo.I.BlueberryBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "Cranberry":
                bush = Instantiate(GlobalInfo.I.CranberryBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "StrawBerry":
                bush = Instantiate(GlobalInfo.I.StrawberryBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "GreenPepper":
                bush = Instantiate(GlobalInfo.I.GreenPepperBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "RedPepper":
                bush = Instantiate(GlobalInfo.I.RedPepperBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "YellowPepper":
                bush = Instantiate(GlobalInfo.I.YellowPepperBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
            case "Potatoe":
                bush = Instantiate(GlobalInfo.I.PotatoeBush, transform);
                bush.GetComponent<Bush>().Dirt = this;
                Occupied = true;
                break;
        }
    }
}
