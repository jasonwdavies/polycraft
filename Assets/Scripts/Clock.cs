﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clock : MonoBehaviour {

    private static Clock _I;
    public static Clock I
    {
        get
        {
            if (_I == null) {
                _I = FindObjectOfType<Clock>();
            }
            return _I;
        }
    }

    public Text DayText;
    public Text ClockText;
    public int TimeMultiplier;
    public int TotalMinutes { get { return (int)_totalMinutes; } }

    private int _minutesDisplayed;
    private int _hoursDisplayed;
    private int _daysDisplayed;
    private float _totalMinutes;
    private bool _afterMidnight;

    void Start() {
        if (TimeMultiplier == 0) {
            TimeMultiplier = 1;
        }
        _afterMidnight = true;
    }

    void Update() {

        _totalMinutes += TimeMultiplier * Time.deltaTime;
        _minutesDisplayed = Mathf.FloorToInt(_totalMinutes % 60f);
        _hoursDisplayed = Mathf.FloorToInt(_totalMinutes / 60f);
        _daysDisplayed = Mathf.FloorToInt(_totalMinutes / 720f);

        // Set the AM / PM flag
        if ((_hoursDisplayed / 12) % 2 == 0) { // assumes we start at the game AM
            _afterMidnight = true;
        }
        else {
            _afterMidnight = false;
        }

        // Logic for cycling back around to 1AM instead of 0AM
        _hoursDisplayed %= 12;
        if (_hoursDisplayed == 0) {
            _hoursDisplayed = 12;
        }

        // Format the text for display
        //DayText.text = "Day " + string.Format("{0:00}", _daysDisplayed);
        DayText.text = "Day " + _daysDisplayed;
        ClockText.text = string.Format("{0:00}:{1:00}", _hoursDisplayed, _minutesDisplayed);
        if (_afterMidnight) {
            ClockText.text += " AM";
        }  else {
            ClockText.text += " PM";
        }
    }

    public bool SunlightHours() {
        return ((_afterMidnight && _hoursDisplayed != 12 && _hoursDisplayed >= GlobalInfo.I.SunriseTime) || (!_afterMidnight && (_hoursDisplayed == 12 || _hoursDisplayed <= GlobalInfo.I.SunsetTime)));
    }
}
