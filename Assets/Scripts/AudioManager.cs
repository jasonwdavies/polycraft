﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public AudioSource[] Music;

    private void Awake() {
        if (PlayerPrefs.HasKey("Music")) {
            Music[PlayerPrefs.GetInt("Music")].Play();
        } else {
            PlayerPrefs.SetInt("Music", 0);
        }
    }

    public void NextMusic() {
        Music[PlayerPrefs.GetInt("Music")].Stop();
        PlayerPrefs.SetInt("Music", (PlayerPrefs.GetInt("Music") + 1) % Music.Length);
        Music[PlayerPrefs.GetInt("Music")].Play();
    }
}
