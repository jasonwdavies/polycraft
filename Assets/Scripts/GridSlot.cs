﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GridSlot {
    public Vector3 Position;
    public int OccupiedId;

    public GridSlot(Vector3 position, int occupiedId) {
        Position = position;
        OccupiedId = occupiedId;
    }
}
