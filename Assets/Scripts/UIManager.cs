﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    private static UIManager _I;
    public static UIManager I
    {
        get
        {
            if (_I == null) {
                _I = FindObjectOfType<UIManager>();
            }
            return _I;
        }
    }

    public GameObject Crosshair;
    public GameObject TooltipUI;
    public Text TooltipText;
    public bool Initialized;

    [Header("Useful Colors")]
    public Color TabEnabledColor;
    public Color TabDisabledColor;
    public Color FlameEnabledColor;
    public Color FlameDisabledColor;
    public Color ArrowEnabledColor;
    public Color ArrowDisabledColor;
    public Color EnergyEnabledColor;
    public Color EnergyDisabledColor;
    public Color RecipeSlotUIEnabledColor;
    public Color RecipeSlotUILockedColor;
    public Color RecipeSlotUIHiddenColor;

    [Header("UIItemSlot Creation")]
    public GameObject UISlot;
    public UIItem MouseSlotUIItem;
    public RectTransform RecipeGridLayout;

    [Header("UI Parents")]
    public GameObject HotbarUI;
    public GameObject ClockUI;
    public GameObject InventoryUI;
    public GameObject TabsUI;

    [Header("HeldItem UI")]
    public SpriteRenderer HeldItemSprite;
    public int InvUISlotSelectedIndex;
    private GameObject[] _inventoryUISlots;

    [Header("Inventories UI")]
    public GameObject BackpackUI;
    public Button BackpackButton;
    public GameObject ChestUI;
    public GameObject[] ChestSlots;
    public RectTransform ChestGridLayout;

    [Header("Crafting NEI UI")]
    public int NumOfRecipes;
    public GameObject CraftingUI;
    public Button CraftingButton;
    public GameObject[] RecipeSlotUI;
    public Image HiddenRecipeCheckmark;
    public GameObject FirepitCategory;
    public GameObject AnvilCategory;
    public GameObject ForgeCategory;
    public GameObject WoodworkCategory;

    [Header("SmartBlock UI")]
    public BlockObject SelectedBlock;
    public GameObject SmartBlockUI;
    public GameObject FirepitUI;
    public GameObject[] FirepitSlots;
    public Image FirepitFlameIcon;
    public Image FirepitArrowIcon;
    public GameObject AnvilUI;
    public GameObject[] AnvilSlots;
    public GameObject ForgeUI;
    public GameObject[] ForgeSlots;
    public Image ForgeFlameIcon;
    public Image ForgeArrowIcon;
    public GameObject GeneratorUI;
    public GameObject[] GeneratorSlots;
    public Image GeneratorEnergyIcon;
    public RectTransform GeneratorEnergyInputBar;
    public RectTransform GeneratorEnergyOutputBar;
    public Text GeneratorRateText;
    public GameObject BarrelUI;
    public GameObject[] BarrelSlots;

    [Header("Nutrition UI")]
    public GameObject NutritionUI;
    public Button NutritionButton;
    public RectTransform EnergyBar;
    public RectTransform HealthBar;
    public RectTransform HungerBar;
    public RectTransform ThirstBar;
    public RectTransform MeatsBar;
    public RectTransform DairyBar;
    public RectTransform GrainsBar;
    public RectTransform VegetablesBar;
    public RectTransform FruitsBar;
    public RectTransform SlotBar;
    public Text EnergyText;
    public Text HealthText;
    public Text HungerText;
    public Text ThirstText;
    public Text MeatsText;
    public Text DairyText;
    public Text GrainsText;
    public Text VegetablesText;
    public Text FruitsText;

    private float _previousEnergyRemaining;
    private float _previousEnergyMax;
    private float _previousHealthRemaining;
    private float _previousHealthMax;
    private float _previousHungerRemaining;
    private float _previousHungerMax;
    private float _previousThirstRemaining;
    private float _previousThirstMax;
    private float _previousMeatsRemaining;
    private float _previousMeatsMax;
    private float _previousDairyRemaining;
    private float _previousDairyMax;
    private float _previousGrainsRemaining;
    private float _previousGrainsMax;
    private float _previousVegetablesRemaining;
    private float _previousVegetablesMax;
    private float _previousFruitsRemaining;
    private float _previousFruitsMax;

    private float _energyBarWidth;
    private float _healthBarWidth;
    private float _hungerBarWidth;
    private float _thirstBarWidth;
    private float _meatsBarWidth;
    private float _dairyBarWidth;
    private float _grainsBarWidth;
    private float _vegetablesBarWidth;
    private float _fruitsBarWidth;
    private float _slotBarWidth;
    private float _generatorEnergyInputBarWidth;
    private float _generatorEnergyOutputBarHeight;

    private bool _hiddenRecipesRevealed;

    private void Start() {
        RecipeSlotUI = new GameObject[Crafting.I.FirepitRecipes.Length + Crafting.I.AnvilRecipes.Length 
                                      + Crafting.I.ForgeRecipes.Length + Crafting.I.WoodworkRecipes.Length 
                                      + Crafting.I.OtherRecipes.Length];
        for (int i=0; i<RecipeSlotUI.Length; i++) {
            //CreateUIRecipeSlot(i);
            CreateUISlot(i, DragItem.SlotType.Recipe, RecipeGridLayout, RecipeSlotUI);
        }

        ChestSlots = new GameObject[20];
        for (int i=0; i<ChestSlots.Length; i++) {
            CreateUISlot(i, DragItem.SlotType.SmartBlock, ChestGridLayout, ChestSlots);
        }

        _energyBarWidth = EnergyBar.sizeDelta.x;
        _healthBarWidth = HealthBar.sizeDelta.x;
        _hungerBarWidth = HungerBar.sizeDelta.x;
        _thirstBarWidth = ThirstBar.sizeDelta.x;
        _meatsBarWidth = MeatsBar.sizeDelta.x;
        _dairyBarWidth = DairyBar.sizeDelta.x;
        _grainsBarWidth = GrainsBar.sizeDelta.x;
        _vegetablesBarWidth = VegetablesBar.sizeDelta.x;
        _fruitsBarWidth = FruitsBar.sizeDelta.x;
        _slotBarWidth = SlotBar.sizeDelta.x;
        _generatorEnergyInputBarWidth = GeneratorEnergyInputBar.sizeDelta.x;
        _generatorEnergyOutputBarHeight = GeneratorEnergyOutputBar.sizeDelta.y;

        InvUISlotSelectedIndex = 0;
        _inventoryUISlots = new GameObject[GlobalInfo.I.LocalPlayer.LocalInventory.Capacity];

        // Grab the HotbarUI slots
        for (int i = 0; i < HotbarUI.transform.childCount; i++) {
            _inventoryUISlots[i] = HotbarUI.transform.GetChild(i).gameObject;
        }

        // Grab the InventoryUI slots
        GameObject backpackGridview = BackpackUI.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
        for (int j = HotbarUI.transform.childCount; j < GlobalInfo.I.LocalPlayer.LocalInventory.Capacity; j++) {
            _inventoryUISlots[j] = backpackGridview.transform.GetChild(j - HotbarUI.transform.childCount).gameObject;
        }

        if (GlobalInfo.I.LocalPlayer.ItemSelected != null) {
            HeldItemSprite.sprite = GlobalInfo.I.LocalPlayer.ItemSelected.Icon;
        }

        UpdateInventoryUI();
        Initialized = true; 
    }

    public void UpdateInventoryUI() {
        UpdateSlotUI(_inventoryUISlots, GlobalInfo.I.LocalPlayer.LocalInventory);
    }

    public void TryUpdateChestUI() {
        //if (SelectedBlock != null && SelectedBlock.BlockInfo != null && SelectedBlock.BlockInfo.Id == 97 && SelectedBlock.BlockInfo.MyInventory != null) {
        if (ChestUI.activeInHierarchy && SelectedBlock != null && SelectedBlock.BlockInfo != null && SelectedBlock.BlockInfo.Id == 97) {
            UpdateSlotUI(ChestSlots, SelectedBlock.BlockInfo.MyInventory);
        }
    }

    public void ChangeSlotSelected(int slotDeselectIndex, int slotSelectIndex) {
        if (slotDeselectIndex != slotSelectIndex) {
            Image slotSelectImage = _inventoryUISlots[slotSelectIndex].GetComponent<Image>();
            Image slotDeselectImage = _inventoryUISlots[slotDeselectIndex].GetComponent<Image>();
            Sprite highlightSprite = slotDeselectImage.sprite;

            slotDeselectImage.sprite = slotSelectImage.sprite;
            slotSelectImage.sprite = highlightSprite;

            InvUISlotSelectedIndex = slotSelectIndex;
            GlobalInfo.I.LocalPlayer.UpdateItemSelected();
        }
    }

    public void UpdateEnergyUI() {
        if (GlobalInfo.I.LocalPlayer.EnergyRemaining != _previousEnergyRemaining || GlobalInfo.I.LocalPlayer.EnergyMax != _previousEnergyMax) {
            EnergyBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.EnergyRemaining, GlobalInfo.I.LocalPlayer.EnergyMax) / GlobalInfo.I.LocalPlayer.EnergyMax) * _energyBarWidth,
                                                EnergyBar.sizeDelta.y);
            EnergyText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.EnergyRemaining) + "/" + GlobalInfo.I.LocalPlayer.EnergyMax;
        }
        _previousEnergyRemaining = GlobalInfo.I.LocalPlayer.EnergyRemaining;
        _previousEnergyMax = GlobalInfo.I.LocalPlayer.EnergyMax;
    }

    public void UpdateHealthUI() {
        if (GlobalInfo.I.LocalPlayer.HealthRemaining != _previousHealthRemaining || GlobalInfo.I.LocalPlayer.HealthMax != _previousHealthMax) {
            HealthBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.HealthRemaining, GlobalInfo.I.LocalPlayer.HealthMax) / GlobalInfo.I.LocalPlayer.HealthMax) * _healthBarWidth,
                                                HealthBar.sizeDelta.y);
            HealthText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.HealthRemaining) + "/" + GlobalInfo.I.LocalPlayer.HealthMax;
        }
        _previousHealthRemaining = GlobalInfo.I.LocalPlayer.HealthRemaining;
        _previousHealthMax = GlobalInfo.I.LocalPlayer.HealthMax;
    }

    public void UpdateDurabilityBarUI() {
        if (GlobalInfo.I.LocalPlayer.EnergyRemaining != _previousEnergyRemaining || GlobalInfo.I.LocalPlayer.EnergyMax != _previousEnergyMax) {
            EnergyBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.EnergyRemaining, GlobalInfo.I.LocalPlayer.EnergyMax) / GlobalInfo.I.LocalPlayer.EnergyMax) * _energyBarWidth,
                                                EnergyBar.sizeDelta.y);
            EnergyText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.EnergyRemaining) + "/" + GlobalInfo.I.LocalPlayer.EnergyMax;
        }
        _previousEnergyRemaining = GlobalInfo.I.LocalPlayer.EnergyRemaining;
        _previousEnergyMax = GlobalInfo.I.LocalPlayer.EnergyMax;
    }

    /// <summary>
    /// Tries to update the visible UI if it is visible
    /// </summary>
    public void TryUpdateUI() {
        if (BackpackUI.activeInHierarchy) {
            UpdateInventoryUI();
        } else if (CraftingUI.activeInHierarchy) {
            UpdateCraftingUI();
        } else if (NutritionUI.activeInHierarchy) {
            UpdateNutritionUI();
        } else if (ChestUI.activeInHierarchy) {
            TryUpdateChestUI();
        }
    }

    /// <summary>
    /// Updates the nutrition UI
    /// </summary>
    public void UpdateNutritionUI() {
        UpdateHungerUI();
        UpdateThirstUI();
        UpdateMeatsUI();
        UpdateDairyUI();
        UpdateGrainsUI();
        UpdateVegetablesUI();
        UpdateFruitsUI();
    }

    public void UpdateHungerUI() {
        if (GlobalInfo.I.LocalPlayer.HungerRemaining != _previousHungerRemaining || GlobalInfo.I.LocalPlayer.HungerMax != _previousHungerMax) {
            HungerBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.HungerRemaining, GlobalInfo.I.LocalPlayer.HungerMax) / GlobalInfo.I.LocalPlayer.HungerMax) * _hungerBarWidth,
                                                HungerBar.sizeDelta.y);
            HungerText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.HungerRemaining) + "/" + GlobalInfo.I.LocalPlayer.HungerMax;
        }
        _previousHungerRemaining = GlobalInfo.I.LocalPlayer.HungerRemaining;
        _previousHungerMax = GlobalInfo.I.LocalPlayer.HungerMax;
    }

    public void UpdateThirstUI() {
        if (GlobalInfo.I.LocalPlayer.ThirstRemaining != _previousThirstRemaining || GlobalInfo.I.LocalPlayer.HungerMax != _previousThirstMax) {
            ThirstBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.ThirstRemaining, GlobalInfo.I.LocalPlayer.ThirstMax) / GlobalInfo.I.LocalPlayer.ThirstMax) * _thirstBarWidth,
                                                ThirstBar.sizeDelta.y);
            ThirstText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.ThirstRemaining) + "/" + GlobalInfo.I.LocalPlayer.ThirstMax;
        }
        _previousThirstRemaining = GlobalInfo.I.LocalPlayer.ThirstRemaining;
        _previousThirstMax = GlobalInfo.I.LocalPlayer.ThirstMax;
    }

    public void UpdateMeatsUI() {
        if (GlobalInfo.I.LocalPlayer.MeatsRemaining != _previousMeatsRemaining || GlobalInfo.I.LocalPlayer.MeatsMax != _previousMeatsMax) {
            MeatsBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.MeatsRemaining, GlobalInfo.I.LocalPlayer.MeatsMax) / GlobalInfo.I.LocalPlayer.MeatsMax) * _meatsBarWidth,
                                                MeatsBar.sizeDelta.y);
            MeatsText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.MeatsRemaining) + "/" + GlobalInfo.I.LocalPlayer.MeatsMax;
        }
        _previousMeatsRemaining = GlobalInfo.I.LocalPlayer.MeatsRemaining;
        _previousMeatsMax = GlobalInfo.I.LocalPlayer.MeatsMax;
    }

    public void UpdateDairyUI() {
        if (GlobalInfo.I.LocalPlayer.DairyRemaining != _previousDairyRemaining || GlobalInfo.I.LocalPlayer.DairyMax != _previousDairyMax) {
            DairyBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.DairyRemaining, GlobalInfo.I.LocalPlayer.DairyMax) / GlobalInfo.I.LocalPlayer.DairyMax) * _dairyBarWidth,
                                                DairyBar.sizeDelta.y);
            DairyText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.DairyRemaining) + "/" + GlobalInfo.I.LocalPlayer.DairyMax;
        }
        _previousDairyRemaining = GlobalInfo.I.LocalPlayer.DairyRemaining;
        _previousDairyMax = GlobalInfo.I.LocalPlayer.DairyMax;
    }

    public void UpdateGrainsUI() {
        if (GlobalInfo.I.LocalPlayer.GrainsRemaining != _previousGrainsRemaining || GlobalInfo.I.LocalPlayer.GrainsMax != _previousGrainsMax) {
            GrainsBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.GrainsRemaining, GlobalInfo.I.LocalPlayer.GrainsMax) / GlobalInfo.I.LocalPlayer.GrainsMax) * _grainsBarWidth,
                                                GrainsBar.sizeDelta.y);
            GrainsText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.GrainsRemaining) + "/" + GlobalInfo.I.LocalPlayer.GrainsMax;
        }
        _previousGrainsRemaining = GlobalInfo.I.LocalPlayer.GrainsRemaining;
        _previousGrainsMax = GlobalInfo.I.LocalPlayer.GrainsMax;
    }

    public void UpdateVegetablesUI() {
        if (GlobalInfo.I.LocalPlayer.VegetablesRemaining != _previousVegetablesRemaining || GlobalInfo.I.LocalPlayer.VegetablesMax != _previousVegetablesMax) {
            VegetablesBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.VegetablesRemaining, GlobalInfo.I.LocalPlayer.VegetablesMax) / GlobalInfo.I.LocalPlayer.VegetablesMax) * _vegetablesBarWidth, 
                                                VegetablesBar.sizeDelta.y);
            VegetablesText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.VegetablesRemaining) + "/" + GlobalInfo.I.LocalPlayer.VegetablesMax;
        }
        _previousVegetablesRemaining = GlobalInfo.I.LocalPlayer.VegetablesRemaining;
        _previousVegetablesMax = GlobalInfo.I.LocalPlayer.VegetablesMax;
    }

    public void UpdateFruitsUI() {
        if (GlobalInfo.I.LocalPlayer.FruitsRemaining != _previousFruitsRemaining || GlobalInfo.I.LocalPlayer.FruitsMax != _previousFruitsMax) {
            FruitsBar.sizeDelta = new Vector2((Mathf.Min(GlobalInfo.I.LocalPlayer.FruitsRemaining, GlobalInfo.I.LocalPlayer.FruitsMax) / GlobalInfo.I.LocalPlayer.FruitsMax) * _fruitsBarWidth,
                                                FruitsBar.sizeDelta.y);
            FruitsText.text = Mathf.FloorToInt(GlobalInfo.I.LocalPlayer.FruitsRemaining) + "/" + GlobalInfo.I.LocalPlayer.FruitsMax;
        }
        _previousFruitsRemaining = GlobalInfo.I.LocalPlayer.FruitsRemaining;
        _previousFruitsMax = GlobalInfo.I.LocalPlayer.FruitsMax;
    }

    public void UpdateCraftingUI() {
        FirepitCategory.SetActive(Crafting.I.AllowFirepitCrafting);
        AnvilCategory.SetActive(Crafting.I.AllowAnvilCrafting);
        ForgeCategory.SetActive(Crafting.I.AllowForgeCrafting);
        WoodworkCategory.SetActive(Crafting.I.AllowWoodworkCrafting);

        ShowAvailableRecipeLists();
    }

    private void ShowAvailableRecipeLists() {
        switch (Crafting.I.MethodSelected) {
            case Crafting.CraftingMethod.Firepit:
                ShowAvailableRecipes(Crafting.CraftingMethod.Firepit);
                break;
            case Crafting.CraftingMethod.Anvil:
                ShowAvailableRecipes(Crafting.CraftingMethod.Anvil);
                break;
            case Crafting.CraftingMethod.Forge:
                ShowAvailableRecipes(Crafting.CraftingMethod.Forge);
                break;
            case Crafting.CraftingMethod.Woodwork:
                ShowAvailableRecipes(Crafting.CraftingMethod.Woodwork);
                break;
            case Crafting.CraftingMethod.Other:
                ShowAvailableRecipes(Crafting.CraftingMethod.Other);
                break;
        }
    }

    private void ShowAvailableRecipes(Crafting.CraftingMethod method) {
        // Reset RecipeSlots
        for (int i=0; i<RecipeSlotUI.Length; i++) {
            RecipeSlotUI[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < Crafting.I.AllRecipes[method].Length; i++) {
            switch (Crafting.I.AllRecipes[method][i].KeyIngredient) {
                case Item.KeyIngredient.None:
                    UpdateRecipeSlotUI(method, i);
                    break;
                case Item.KeyIngredient.Wood:
                    if (Crafting.I.HasWood) {
                        UpdateRecipeSlotUI(method, i);
                    }
                    break;
                case Item.KeyIngredient.Stone:
                    if (Crafting.I.HasStone) {
                        UpdateRecipeSlotUI(method, i);
                    }
                    break;
                case Item.KeyIngredient.Copper:
                    if (Crafting.I.HasCopper) {
                        UpdateRecipeSlotUI(method, i);
                    }
                    break;
            }
        }

        int numOfVisibleRecipes = 0;
        foreach (Transform child in RecipeGridLayout) {
            if (child.gameObject.activeInHierarchy) {
                numOfVisibleRecipes++;
            }
        }
        if (numOfVisibleRecipes > 15) {
            RecipeGridLayout.sizeDelta = new Vector2(numOfVisibleRecipes * 30, RecipeGridLayout.sizeDelta.y);
        }
    }

    private void UpdateRecipeSlotUI(Crafting.CraftingMethod method, int recipeIndex) {
        Recipe recipe = Crafting.I.AllRecipes[method][recipeIndex];
        UIItem uiItem = RecipeSlotUI[recipeIndex].transform.GetChild(0).gameObject.GetComponent<UIItem>();
        Image uiItemImage = uiItem.gameObject.GetComponent<Image>();

        uiItem.SetCraftingUIItem(ItemManager.I.GetItem(recipe.ResultID));

        if (Crafting.I.CheckForIngredients(recipe, GlobalInfo.I.LocalPlayer.LocalInventory)) {
            RecipeSlotUI[recipeIndex].gameObject.SetActive(true);
            if (recipe.TimeToCraft == 0) {
                Crafting.I.AllRecipes[method][recipeIndex].IsCraftable = true;
                uiItemImage.color = RecipeSlotUIEnabledColor;
            } else {
                Crafting.I.AllRecipes[method][recipeIndex].IsCraftable = false;
                uiItemImage.color = RecipeSlotUILockedColor;
            }
        } else {
            if (_hiddenRecipesRevealed) {
                RecipeSlotUI[recipeIndex].gameObject.SetActive(true);
            }
            Crafting.I.AllRecipes[method][recipeIndex].IsCraftable = false;
            uiItemImage.color = RecipeSlotUIHiddenColor;
        }
    }

    public void ChangeTabs(int index) {
        switch (index) {
            case 0:
                BackpackUI.SetActive(true);
                CraftingUI.SetActive(false);
                NutritionUI.SetActive(false);
                UpdateInventoryUI();
                break;
            case 1:
                BackpackUI.SetActive(false);
                //BackpackButton.Color = TabDisabledColor;
                CraftingUI.SetActive(true);
                //CraftingButton.Color = TabEnabledColor;
                NutritionUI.SetActive(false);
                //NutritionButton.Color = TabDisabledColor;
                UpdateCraftingUI();
                break;
            case 2:
                BackpackUI.SetActive(false);
                //BackpackButton.Color = TabDisabledColor;
                CraftingUI.SetActive(false);
                //CraftingButton.Color = TabDisabledColor;
                NutritionUI.SetActive(true);
                //NutritionButton.Color = TabEndabledColor;
                UpdateNutritionUI();
                break;
        }
    }

    public void SelectCraftingMethod(int method) {
        Crafting.I.MethodSelected = (Crafting.CraftingMethod)method;
        UpdateCraftingUI();
    }

    public void ToggleHiddenRecipes() {
        if (_hiddenRecipesRevealed) {
            _hiddenRecipesRevealed = false;
            HiddenRecipeCheckmark.enabled = false;
        } else {
            _hiddenRecipesRevealed = true;
            HiddenRecipeCheckmark.enabled = true;
        }
        UpdateCraftingUI();
    }

    public void CreateUISlot(int slotNumber, DragItem.SlotType type, RectTransform gridLayout, GameObject[] slotUI) {
        GameObject newSlotObj = Instantiate(UISlot);
        DragItem dragItem = newSlotObj.GetComponent<DragItem>();

        dragItem.SlotNumber = slotNumber;
        dragItem.Type = type;
        dragItem.MouseSlotItem = MouseSlotUIItem;
        newSlotObj.transform.SetParent(gridLayout);
        newSlotObj.name = "Slot" + dragItem.SlotNumber;
        newSlotObj.transform.localScale = new Vector3(1, 1, 1); // to prevent the override of scale from GridLayout
        slotUI[dragItem.SlotNumber] = newSlotObj;
    }

    public void UpdateSlotUI(GameObject[] slots, Inventory inventory) {
        Transform slotTransform;
        Image slotImage;
        Image slotBar;
        Image slotBarBackground;
        Text slotNumber;
        Item slotItem;

        for (int i=0; i<slots.Length; i++) {
            slotTransform = slots[i].transform;
            slotImage = slotTransform.GetChild(0).gameObject.GetComponent<Image>(); // the UIItem Image
            slotBarBackground = slotTransform.GetChild(0).GetChild(0).gameObject.GetComponent<Image>(); // The Bar's Background
            slotBar = slotTransform.GetChild(0).GetChild(1).gameObject.GetComponent<Image>(); // The Bar
            slotNumber = slotTransform.GetChild(0).GetChild(2).gameObject.GetComponent<Text>(); // The Quantity
            slotTransform.GetChild(0).gameObject.GetComponent<UIItem>().MyItem = inventory.Items[i]; // Set UIItem's reference to it's item         // In case of NRE, inventory is null, & Array Out of Bounds is mismatch between slots.Length and Items.Length
            slotItem = slotTransform.GetChild(0).gameObject.GetComponent<UIItem>().MyItem;
            if (inventory.Items[i] == null) {
                slotImage.color = new Color(slotImage.color.r, slotImage.color.g, slotImage.color.b, 0);
                slotBarBackground.color = new Color(slotBarBackground.color.r, slotBarBackground.color.g, slotBarBackground.color.b, 0);
                slotBar.color = new Color(slotBar.color.r, slotBar.color.g, slotBar.color.b, 0);
                slotNumber.color = new Color(slotNumber.color.r, slotNumber.color.g, slotNumber.color.b, 0);
            } else {
                slotImage.color = new Color(slotImage.color.r, slotImage.color.g, slotImage.color.b, 255);
                slotImage.sprite = inventory.Items[i].Icon;
                slotBarBackground.color = new Color(slotBarBackground.color.r, slotBarBackground.color.g, slotBarBackground.color.b, 255);
                slotBar.color = new Color(slotBar.color.r, slotBar.color.g, slotBar.color.b, 255);
                slotBar.rectTransform.sizeDelta = new Vector2((Mathf.Min(slotItem.LifeSpanRemaining, slotItem.LifeSpan) / slotItem.LifeSpan) * _slotBarWidth, SlotBar.sizeDelta.y);
                slotNumber.color = new Color(slotNumber.color.r, slotNumber.color.g, slotNumber.color.b, 255);
                slotNumber.text = inventory.Items[i].Quantity.ToString();
            }
        }
    }
    
    public void UpdateItemSelectedSprite() {
        if (GlobalInfo.I.LocalPlayer.ItemSelected != null) {
            HeldItemSprite.sprite = GlobalInfo.I.LocalPlayer.ItemSelected.Icon;
        } else {
            HeldItemSprite.sprite = null;
        }
    }

    public void UpdateFirepitUI(bool firepitActive) {
        if (SelectedBlock != null && SelectedBlock.BlockInfo is SmartBlock && FirepitUI.activeInHierarchy && SmartBlockUI.activeInHierarchy) {
            if (firepitActive) {
                FirepitFlameIcon.color = FlameEnabledColor;
                FirepitArrowIcon.color = ArrowEnabledColor;
            } else {
                FirepitFlameIcon.color = FlameDisabledColor;
                FirepitArrowIcon.color = ArrowDisabledColor;
            }
        }
    }

    public void UpdateForgeUI(bool forgeActive) {
        if (SelectedBlock != null && SelectedBlock.BlockInfo is SmartBlock && ForgeUI.activeInHierarchy && SmartBlockUI.activeInHierarchy) {
            if (forgeActive) {
                ForgeFlameIcon.color = FlameEnabledColor;
                ForgeArrowIcon.color = ArrowEnabledColor;
            } else {
                ForgeFlameIcon.color = FlameDisabledColor;
                ForgeArrowIcon.color = ArrowDisabledColor;
            }
        }
    }

    public void UpdateGeneratorUI(bool generatorActive) {
        if (SelectedBlock != null && SelectedBlock.BlockInfo is SmartBlock && GeneratorUI.activeInHierarchy && SmartBlockUI.activeInHierarchy) {
            GeneratorEnergyInputBar.sizeDelta = new Vector2((Mathf.Min((float)SelectedBlock.BlockInfo.EnergyCurrent, (float)SelectedBlock.BlockInfo.EnergyMax) / (float)SelectedBlock.BlockInfo.EnergyMax)
                                                            * _generatorEnergyInputBarWidth, GeneratorEnergyInputBar.sizeDelta.y);
            GeneratorRateText.text = SelectedBlock.BlockInfo.EnergyRateGenerated - SelectedBlock.BlockInfo.EnergyRateConsumed + "W/sec";

            if (generatorActive) {
                GeneratorEnergyIcon.color = EnergyEnabledColor;
            } else {
                GeneratorEnergyIcon.color = EnergyDisabledColor;
            }
        }
    }
}
