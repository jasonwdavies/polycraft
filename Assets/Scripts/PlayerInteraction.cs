﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInteraction : MonoBehaviour {

    private float _itemUseStartTime;

    void Start () {
        if (gameObject.GetComponent<SphereCollider>() == null) {
            SphereCollider sc = gameObject.AddComponent<SphereCollider>();
            sc.radius = 1.2f;
        }
    }

    private void Update() {
        // Hotbar slot selection interaction
        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 0);
        } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 1);
        } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 2);
        } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 3);
        } else if (Input.GetKeyDown(KeyCode.Alpha5)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 4);
        } else if (Input.GetKeyDown(KeyCode.Alpha6)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 5);
        } else if (Input.GetKeyDown(KeyCode.Alpha7)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 6);
        } else if (Input.GetKeyDown(KeyCode.Alpha8)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 7);
        } else if (Input.GetKeyDown(KeyCode.Alpha9)) {
            UIManager.I.ChangeSlotSelected(UIManager.I.InvUISlotSelectedIndex, 8);
        }

        // Inventory UI toggle
        if (Input.GetKeyDown(KeyCode.I)) {
            //if (UIManager.I.InventoryUI.activeInHierarchy) {
            if (GlobalInfo.I.LocalPlayer.IsViewingUI) {
                UIManager.I.TabsUI.SetActive(false);
                UIManager.I.InventoryUI.SetActive(false);
                UIManager.I.SmartBlockUI.SetActive(false);
                UIManager.I.ClockUI.SetActive(false);
                GlobalInfo.I.LocalPlayer.IsViewingUI = false;
                GlobalInfo.I.LocalPlayer.ToggleCursor();
            } else {
                GlobalInfo.I.LocalPlayer.IsViewingUI = true;
                GlobalInfo.I.LocalPlayer.ToggleCursor();
                UIManager.I.TabsUI.SetActive(true);
                UIManager.I.InventoryUI.SetActive(true);
                UIManager.I.ClockUI.SetActive(true);
                UIManager.I.TryUpdateUI();
            }
        }

        // Attempting to open/close a SmartBlock or Chest
        if (Input.GetMouseButtonDown(1)) {
            if (GlobalInfo.I.LocalPlayer.IsViewingUI) {
                for (int i = 0; i < UIManager.I.SmartBlockUI.transform.childCount; i++) {
                    UIManager.I.SmartBlockUI.transform.GetChild(i).gameObject.SetActive(false);
                }
                UIManager.I.InventoryUI.SetActive(false);
                UIManager.I.SmartBlockUI.SetActive(false);
                GlobalInfo.I.LocalPlayer.IsViewingUI = false;
                UIManager.I.SelectedBlock = null;
            } else {
                RaycastHit hit;
                Ray ray = new Ray(GlobalInfo.I.LocalCamera.gameObject.transform.position + GlobalInfo.I.LocalCamera.gameObject.transform.forward * 0.5f,
                                  GlobalInfo.I.LocalCamera.gameObject.transform.forward * 3);
                if (Physics.Raycast(ray, out hit)) {
                    UIManager.I.SelectedBlock = hit.transform.gameObject.GetComponent<BlockObject>();
                    if (UIManager.I.SelectedBlock != null) {
                        UIManager.I.InventoryUI.SetActive(true);
                        UIManager.I.TabsUI.SetActive(false);
                        UIManager.I.BackpackUI.SetActive(true);
                        UIManager.I.NutritionUI.SetActive(false);
                        UIManager.I.CraftingUI.SetActive(false);
                        GlobalInfo.I.LocalPlayer.IsViewingUI = true;
                        if (UIManager.I.SelectedBlock.BlockInfo is SmartBlock) {
                            UIManager.I.SmartBlockUI.SetActive(true);
                            switch (UIManager.I.SelectedBlock.BlockInfo.Id) {
                                case 97:
                                    UIManager.I.ChestUI.SetActive(true);
                                    UIManager.I.TryUpdateChestUI();
                                    break;
                                case 111:
                                    if (Crafting.I.AllowAnvilCrafting) {
                                        UIManager.I.AnvilUI.SetActive(true);
                                        ((SmartBlock)(UIManager.I.SelectedBlock.BlockInfo)).TryDelayedCrafting();
                                        UIManager.I.UpdateSlotUI(UIManager.I.AnvilSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                    }
                                    break;
                                case 123:
                                    if (Crafting.I.AllowForgeCrafting) {
                                        UIManager.I.ForgeUI.SetActive(true);
                                        ((SmartBlock)(UIManager.I.SelectedBlock.BlockInfo)).TryDelayedCrafting();
                                        UIManager.I.UpdateSlotUI(UIManager.I.ForgeSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                    }
                                    break;
                                case 124:
                                    if (Crafting.I.AllowFirepitCrafting) {
                                        UIManager.I.FirepitUI.SetActive(true);
                                        ((SmartBlock)(UIManager.I.SelectedBlock.BlockInfo)).TryDelayedCrafting();
                                        UIManager.I.UpdateSlotUI(UIManager.I.FirepitSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                    }
                                    break;
                                case 151:
                                case 209:
                                case 210:
                                    UIManager.I.GeneratorUI.SetActive(true);
                                    ((SmartBlock)(UIManager.I.SelectedBlock.BlockInfo)).TryDelayedCrafting();
                                    UIManager.I.UpdateSlotUI(UIManager.I.GeneratorSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                    break;
                            }
                        }
                    }
                }
            }
            GlobalInfo.I.LocalPlayer.ToggleCursor();
        }
    }

    private void OnTriggerEnter(Collider other) {
        ItemContainer itemContainer = other.gameObject.GetComponent<ItemContainer>();
        BlockObject smartBlockObject = other.gameObject.GetComponent<BlockObject>();

        // Hide the item's sprite
        if (itemContainer != null && itemContainer.AllowPickup && itemContainer.PickupItem != null) {
            GlobalInfo.I.LocalPlayer.LocalInventory.AddItem(new Item(itemContainer.PickupItem));
            Destroy(other.gameObject);
            return;
        } else if (smartBlockObject != null && smartBlockObject.BlockInfo != null && smartBlockObject.BlockInfo is SmartBlock) {
            switch (((SmartBlock)(smartBlockObject.BlockInfo)).CraftingBenchType) {
                case Crafting.CraftingMethod.Firepit:
                    Crafting.I.AllowFirepitCrafting = true;
                    break;
                case Crafting.CraftingMethod.Anvil:
                    Crafting.I.AllowAnvilCrafting = true;
                    break;
                case Crafting.CraftingMethod.Forge:
                    Crafting.I.AllowForgeCrafting = true;
                    break;
                case Crafting.CraftingMethod.Woodwork:
                    Crafting.I.AllowWoodworkCrafting = true;
                    break;
            }
            UIManager.I.UpdateCraftingUI();
        }
    }

    private void OnTriggerExit(Collider other) {
        BlockObject smartBlockObject = other.gameObject.GetComponent<BlockObject>();

        if (smartBlockObject != null && smartBlockObject.BlockInfo != null && smartBlockObject.BlockInfo is SmartBlock) {
            switch (((SmartBlock)(smartBlockObject.BlockInfo)).CraftingBenchType) {
                case Crafting.CraftingMethod.Firepit:
                    Crafting.I.AllowFirepitCrafting = false;
                    if (UIManager.I.FirepitUI.activeInHierarchy) {
                        GlobalInfo.I.LocalPlayer.IsViewingUI = false;
                        UIManager.I.FirepitUI.SetActive(false);
                    }
                    if (Crafting.I.MethodSelected == Crafting.CraftingMethod.Firepit) {
                        Crafting.I.MethodSelected = Crafting.CraftingMethod.Other;
                    }
                    break;
                case Crafting.CraftingMethod.Anvil:
                    Crafting.I.AllowAnvilCrafting = false;
                    if (UIManager.I.AnvilUI.activeInHierarchy) {
                        GlobalInfo.I.LocalPlayer.IsViewingUI = false;
                        UIManager.I.AnvilUI.SetActive(false);
                    }
                    if (Crafting.I.MethodSelected == Crafting.CraftingMethod.Anvil) {
                        Crafting.I.MethodSelected = Crafting.CraftingMethod.Other;
                    }
                    break;
                case Crafting.CraftingMethod.Forge:
                    Crafting.I.AllowForgeCrafting = false;
                    if (UIManager.I.ForgeUI.activeInHierarchy) {
                        GlobalInfo.I.LocalPlayer.IsViewingUI = false;
                        UIManager.I.ForgeUI.SetActive(false);
                    }
                    if (Crafting.I.MethodSelected == Crafting.CraftingMethod.Forge) {
                        Crafting.I.MethodSelected = Crafting.CraftingMethod.Other;
                    }
                    break;
                case Crafting.CraftingMethod.Woodwork:
                    break;
            }
            UIManager.I.UpdateCraftingUI();
        }
    }

    private void OnTriggerStay(Collider other) {
        if (Input.GetMouseButtonDown(0)) {
            if (GlobalInfo.I.LocalPlayer.ItemSelected != null && GlobalInfo.I.LocalPlayer.ItemSelected.IsConsumable) {
                return;
            }
            FruitTree fruitTree = other.gameObject.GetComponent<FruitTree>();
            if (fruitTree != null) {
                fruitTree.HarvestFruit();
            }
            Bush bush = other.gameObject.GetComponent<Bush>();
            if (bush != null) {
                bush.HarvestPlant();
                bush.Dirt.Occupied = false;
            }
            TilledDirt tilledDirt = other.gameObject.GetComponent<TilledDirt>();
            if (tilledDirt != null && !tilledDirt.Occupied && GlobalInfo.I.LocalPlayer.ItemSelected != null && GlobalInfo.I.LocalPlayer.ItemSelected.IsSeed) {
                tilledDirt.PlantSeed(GlobalInfo.I.LocalPlayer.ItemSelected);
                GlobalInfo.I.LocalPlayer.LocalInventory.RemoveItem(UIManager.I.InvUISlotSelectedIndex, 1);
            }
        }
    }
}
