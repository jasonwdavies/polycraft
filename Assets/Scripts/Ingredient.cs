﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ingredient { //: System.Object {
    public int ItemID;
    public int Quantity;

    public Ingredient(int itemID, int quantity) {
        ItemID = itemID;
        Quantity = quantity;
    }
}
