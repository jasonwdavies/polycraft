﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class CSVReader : MonoBehaviour {

    private static CSVReader _I;
    public static CSVReader I {
        get {
            if (_I == null) {
                _I = FindObjectOfType<CSVReader>();
            }
            return _I;
        }
    }
    public TextAsset ItemsFile;
    public TextAsset BlocksFile;
    public TextAsset RecipesFile;
    
    private int _fileRows;
    private int _additionalFileRows;
    private List<Recipe> _firepitRecipes = new List<Recipe>();
    private List<Recipe> _anvilRecipes = new List<Recipe>();
    private List<Recipe> _forgeRecipes = new List<Recipe>();
    private List<Recipe> _woodworkRecipes = new List<Recipe>();
    private List<Recipe> _generatorRecipes = new List<Recipe>();
    private List<Recipe> _otherRecipes = new List<Recipe>();

    enum IsBlockType { None, Block, Smart }
    private IsBlockType _isBlockType;

    public void Awake() {
        string[,] blockGrid;
        string[,] grid;

        //#if UNITY_EDITOR
        //IconsFilePath = Application.dataPath;
        //#endif

        //#if UNITY_STANDALONE_WIN
        //IconsFilePath = Application.dataPath + "/Assets/";
        //#endif

        // Items
        grid = SplitCsvGrid(ItemsFile.text, false);
        blockGrid = SplitCsvGrid(BlocksFile.text, true);
        ItemManager.I.Items = new Item[_fileRows - 1];
        //DebugOutputGrid(grid);
        CreateItems(grid, blockGrid);

        // Recipes
        grid = SplitCsvGrid(RecipesFile.text, false);
        //DebugOutputGrid(grid);
        CreateRecipes(grid);
        Crafting.I.OtherRecipes = _otherRecipes.ToArray();
        Crafting.I.FirepitRecipes = _firepitRecipes.ToArray();
        Crafting.I.AnvilRecipes = _anvilRecipes.ToArray();
        Crafting.I.ForgeRecipes = _forgeRecipes.ToArray();
        Crafting.I.WoodworkRecipes = _woodworkRecipes.ToArray();
        Crafting.I.GeneratorRecipes = _generatorRecipes.ToArray();
    }

    public void CreateItems(string[,] grid, string[,] blockGrid) { //, string[,] blockGrid) {
        string name;
        Sprite icon;
        int id;
        int quantity = 1;
        int growsIntoID;
        int maxConsumableStack;
        int slashingDamage;
        int piercingDamage;
        int crushingDamage;
        int armorAmount;
        int containerAmount;
        int inventorySize;
        int tankSize;
        int generatorAmount;
        int batterySize;
        int liquidTankSize;
        int energyRateGenerated;
        int energyRateConsumed;
        int energyMax;
        int[] fuelSlotIndices;
        int[] inputSlotIndices;
        int[] outputSlotIndices;
        float lifeSpan;
        float lifeSpanRemaining;
        float timeToUse;
        float timeCreated = 0;
        float timeToRemove;
        bool isSeed;
        bool isConsumable;
        bool isWeapon;
        bool isArmor;
        bool isContainer;
        bool canCarryLava;
        bool canCarryLiquidMetal;
        bool isPlaced;
        bool isUpgradeable;
        bool isInventory;
        bool isLiquidTank;
        bool isGasTank;
        bool isGenerator;
        bool isBattery;
        Effect consumeEffect;
        Item.KeyIngredient isKeyIngredientType;
        Block blockInfo = null;
        Inventory inventory;
        Block.GeneratorType isGeneratorType;
        Crafting.CraftingMethod craftingBenchType;

        for (int i = 0; i < _fileRows-1; i++) { // start at row 1 (not 0 - header)
            name = grid[1, i];
            if (String.IsNullOrEmpty(name) || name.Equals("Name")) {
                ItemManager.I.Items[i] = null;
            } else {
                // basic
                if (String.IsNullOrEmpty(grid[5, i])) {
                    icon = Resources.Load<Sprite>("Icons/Lemon");
                } else {
                    icon = Resources.Load<Sprite>("Icons/" + grid[5, i]);
                }
                int.TryParse(grid[0, i], out id);
                isKeyIngredientType = Item.KeyIngredient.None;

                // consumables
                float.TryParse(grid[3, i], out lifeSpan);
                float.TryParse(grid[3, i], out lifeSpanRemaining);
                float.TryParse(grid[4, i], out timeToUse);
                isConsumable = grid[5, i].Equals("TRUE");
                int[] recover = new int[9];
                for (int j=0; j<9; j++) { // 9 is the number of recovery categories
                    if (!grid[j+5, i].Equals("0")) { // 5 is the offset to the start
                        int.TryParse(grid[j + 5, i], out recover[j]);
                    } else {
                        recover[j] = 0;
                    }
                }
                consumeEffect = new Effect(recover[0], recover[1], recover[2], recover[3], recover[4],
                                           recover[5], recover[6], recover[7], recover[8]);
                int.TryParse(grid[2, i], out maxConsumableStack);

                // seeds
                isSeed = false;
                growsIntoID = 0;

                // weapons
                isWeapon = grid[15, i].Equals("TRUE");
                int.TryParse(grid[16, i], out slashingDamage);
                int.TryParse(grid[17, i], out piercingDamage);
                int.TryParse(grid[18, i], out crushingDamage);

                // armor
                isArmor = grid[19, i].Equals("TRUE");
                int.TryParse(grid[20, i], out armorAmount);

                // containers
                int.TryParse(grid[21, i], out containerAmount);
                if (containerAmount != 0) {
                    isContainer = true;
                } else {
                    isContainer = false;
                }
                canCarryLava = false; //grid[22, i].Equals("TRUE");
                canCarryLiquidMetal = false; //grid[23, i].Equals("TRUE");

                // blocks
                int locateBlockById;
                blockInfo = null;
                if (!grid[25, i].Equals("NONE")) {
                    for (int j = 0; j < _additionalFileRows; j++) {
                        int.TryParse(blockGrid[0, j], out locateBlockById);
                        if (locateBlockById == id) {
                            isPlaced = false;
                            isUpgradeable = blockGrid[3, j].Equals("TRUE");
                            float.TryParse(blockGrid[12, j], out timeToRemove);
                            isInventory = blockGrid[4, j].Equals("TRUE");
                            int.TryParse(blockGrid[6, j], out inventorySize);
                            if (inventorySize > 0) {
                                inventory = new Inventory(inventorySize);
                                inventory.Items = new Item[inventorySize];
                            }
                            else {
                                inventory = null;
                            }
                            int.TryParse(blockGrid[7, j], out liquidTankSize);
                            isLiquidTank = (liquidTankSize > 0);
                            isGeneratorType = Block.GeneratorType.None;
                            isGenerator = blockGrid[3, j].Equals("TRUE");
                            int.TryParse(blockGrid[10, j], out generatorAmount);
                            int.TryParse(blockGrid[11, j], out batterySize);
                            isBattery = (batterySize > 0);
                            craftingBenchType = Crafting.CraftingMethod.Other;
                            fuelSlotIndices = null;
                            inputSlotIndices = null;
                            outputSlotIndices = null;
                            switch (blockGrid[13, j]) {
                                case "Firepit":
                                    craftingBenchType = Crafting.CraftingMethod.Firepit;
                                    fuelSlotIndices = new int[1] { 0 };
                                    inputSlotIndices = new int[1] { 1 };
                                    outputSlotIndices = new int[1] { 2 };
                                    break;
                                case "Anvil":
                                    craftingBenchType = Crafting.CraftingMethod.Anvil;
                                    inputSlotIndices = new int[1] { 1 };
                                    outputSlotIndices = new int[1] { 0 };
                                    break;
                                case "Forge":
                                    craftingBenchType = Crafting.CraftingMethod.Forge;
                                    fuelSlotIndices = new int[1] { 0 };
                                    inputSlotIndices = new int[1] { 1 };
                                    outputSlotIndices = new int[1] { 2 };
                                    break;
                                case "Generator":
                                    craftingBenchType = Crafting.CraftingMethod.Generator;
                                    fuelSlotIndices = new int[1] { 0 };
                                    inputSlotIndices = new int[1] { 1 };
                                    outputSlotIndices = new int[1] { 1 };
                                    break;
                            }
                            switch (blockGrid[2, j]) {
                                case "BLOCK":
                                    blockInfo = new Block(id, isPlaced, isUpgradeable, timeToRemove, isInventory, inventorySize,
                                                          inventory, isLiquidTank, false, liquidTankSize, isGenerator,
                                                          generatorAmount, 0, batterySize, 0);
                                    break;
                                case "SMART":
                                    blockInfo = new SmartBlock(id, isPlaced, isUpgradeable, timeToRemove, isInventory, inventorySize,
                                                               inventory, isLiquidTank, false, liquidTankSize, isGenerator,
                                                               generatorAmount, 0, batterySize, 0, craftingBenchType, fuelSlotIndices, inputSlotIndices, outputSlotIndices);
                                    break;
                            }
                            break;
                        }
                    }
                }
                ItemManager.I.Items[i] = new Item(name, icon, id, quantity, lifeSpan, lifeSpanRemaining, timeToUse, 
                                      isKeyIngredientType, isConsumable, consumeEffect, maxConsumableStack,
                                      timeCreated, isSeed, growsIntoID, isWeapon, slashingDamage, 
                                      piercingDamage, crushingDamage, isArmor, armorAmount, isContainer, 
                                      canCarryLava, canCarryLiquidMetal, containerAmount, blockInfo);
            }
        }
    }

    public void CreateRecipes(string[,] grid) {
        int id;
        string name;
        Crafting.CraftingMethod craftingMethod;
        Item.KeyIngredient keyIngredient;
        int resultID;
        int itemID;
        int quantity;
        Recipe recipe;
        float timeToCraft;

        for (int i = 1; i < _fileRows; i++) { // i is the row
            name = grid[2, i];
            if (!String.IsNullOrEmpty(name)) {
                int.TryParse(grid[0, i], out id);

                switch (grid[4, i]) {
                    case "Craft":
                        craftingMethod = Crafting.CraftingMethod.Other;
                        break;
                    case "Firepit":
                        craftingMethod = Crafting.CraftingMethod.Firepit;
                        break;
                    case "Anvil":
                        craftingMethod = Crafting.CraftingMethod.Anvil;
                        break;
                    case "Forge":
                        craftingMethod = Crafting.CraftingMethod.Forge;
                        break;
                    case "Generator Block":
                    case "Solar Generator":
                    case "Advanced Solar Generator":
                        craftingMethod = Crafting.CraftingMethod.Generator;
                        break;
                    case "Bloomery":
                        craftingMethod = Crafting.CraftingMethod.Other;
                        break;
                    case "Blast Furnace":
                        craftingMethod = Crafting.CraftingMethod.Other;
                        break;
                    default:
                        craftingMethod = Crafting.CraftingMethod.Other;
                        break;
                }
                float.TryParse(grid[6, i], out timeToCraft);
                keyIngredient = Item.KeyIngredient.None;
                int.TryParse(grid[1, i], out resultID);

                Ingredient[] ingredients = new Ingredient[9];
                int.TryParse(grid[7, i], out itemID);
                int.TryParse(grid[9, i], out quantity);
                ingredients[0] = new Ingredient(itemID, quantity);
                int.TryParse(grid[10, i], out itemID);
                int.TryParse(grid[12, i], out quantity);
                ingredients[1] = new Ingredient(itemID, quantity);
                int.TryParse(grid[13, i], out itemID);
                int.TryParse(grid[15, i], out quantity);
                ingredients[2] = new Ingredient(itemID, quantity);
                int.TryParse(grid[16, i], out itemID);
                int.TryParse(grid[18, i], out quantity);
                ingredients[3] = new Ingredient(itemID, quantity);
                int.TryParse(grid[19, i], out itemID);
                int.TryParse(grid[21, i], out quantity);
                ingredients[4] = new Ingredient(itemID, quantity);
                int.TryParse(grid[22, i], out itemID);
                int.TryParse(grid[24, i], out quantity);
                ingredients[5] = new Ingredient(itemID, quantity);
                int.TryParse(grid[25, i], out itemID);
                int.TryParse(grid[27, i], out quantity);
                ingredients[6] = new Ingredient(itemID, quantity);
                int.TryParse(grid[28, i], out itemID);
                int.TryParse(grid[30, i], out quantity);
                ingredients[7] = new Ingredient(itemID, quantity);
                int.TryParse(grid[31, i], out itemID);
                int.TryParse(grid[33, i], out quantity);
                ingredients[8] = new Ingredient(itemID, quantity);

                recipe = new Recipe(id, name, craftingMethod, keyIngredient, resultID, ingredients, timeToCraft);

                switch (craftingMethod) {
                    case Crafting.CraftingMethod.Other:
                        _otherRecipes.Add(recipe);
                        break;
                    case Crafting.CraftingMethod.Firepit:
                        _firepitRecipes.Add(recipe);
                        break;
                    case Crafting.CraftingMethod.Anvil:
                        _anvilRecipes.Add(recipe);
                        break;
                    case Crafting.CraftingMethod.Forge:
                        _forgeRecipes.Add(recipe);
                        break;
                    case Crafting.CraftingMethod.Woodwork:
                        _woodworkRecipes.Add(recipe);
                        break;
                    case Crafting.CraftingMethod.Generator:
                        _generatorRecipes.Add(recipe);
                        break;
                }
            }
        }
    }

    // outputs the content of a 2D array, useful for checking the importer
    public void DebugOutputGrid(string[,] grid) {
        string textOutput = "";
        for (int y = 0; y < grid.GetUpperBound(1); y++) {
            for (int x = 0; x < grid.GetUpperBound(0); x++) {

                textOutput += grid[x, y];
                textOutput += "|";
            }
            textOutput += "\n";
        }

        Debug.Log(textOutput);
    }

    // splits a CSV file into a 2D string array
    public string[,] SplitCsvGrid(string csvText, bool isAdditionalFile) {
        string[] lines = csvText.Split("\n"[0]);
        if (isAdditionalFile) {
            _additionalFileRows = lines.Length;
        } else {
            _fileRows = lines.Length;
        }

        // finds the max width of row
        int width = 0;
        for (int i = 0; i < lines.Length; i++) {
            string[] row = SplitCsvLine(lines[i]);
            width = Math.Max(width, row.Length);
        }
        
        // creates new 2D string grid to output to
        string[,] outputGrid = new string[width + 1, lines.Length + 1];
        for (int y = 0; y < lines.Length; y++) {
            string[] row = SplitCsvLine(lines[y]);
            for (int x = 0; x < row.Length; x++) {
                outputGrid[x, y] = row[x];

                // This line was to replace "" with " in my output. 
                // Include or edit it as you wish.
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }
        return outputGrid;
    }

    // splits a CSV row 
    public string[] SplitCsvLine(string line) {
        return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
        @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
        System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
        select m.Groups[1].Value).ToArray();
    }
}
