﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour {

    public bool IsDraggingUI; // e.g. dragging UIItem between slots
    public bool IsViewingUI; // e.g. opened inventory

    [Header("Energy")]
    public float EnergyMax;
    public float EnergyRemaining;
    public float EnergyToRun;
    public float EnergyToJump;
    public float EnergyRegen;
    public float EnergyRegenDelay;

    [Header("Health")]
    public float HealthMax;
    public float HealthRemaining;

    [Header("Nutrition")]
    public float UpdateNutritionDelay;
    public float HungerMax;
    public float HungerRemaining;
    public float HungerRate;
    public float ThirstMax;
    public float ThirstRemaining;
    public float ThirstRate;
    public float MeatsMax;
    public float MeatsRemaining;
    public float MeatsRate;
    public float DairyMax;
    public float DairyRemaining;
    public float DairyRate;
    public float GrainsMax;
    public float GrainsRemaining;
    public float GrainsRate;
    public float VegetablesMax;
    public float VegetablesRemaining;
    public float VegetablesRate;
    public float FruitsMax;
    public float FruitsRemaining;
    public float FruitsRate;

    [Header("Inventory")]
    public Inventory LocalInventory; // set in the editor
    public Item ItemSelected;

    [Header("Blocks")]
    public GameObject SelectedBlockObj;

    private FirstPersonController _firstPersonController;
    private CharacterController _characterController;
    private float _energyDelayTimer;
    private float _runSpeed;
    private float _previousNutritionCheck;
    private float _itemUseStartTime;
    private float _blockRemoveStartTime;
    private float _timeOfLastPlacementAttempt;

    public void Awake() {
        LocalInventory = new Inventory(30);
    }

    public void Start() {
        EnergyRemaining = EnergyMax;
        HealthRemaining = HealthMax;
        HealthRemaining = HealthMax;
        MeatsRemaining = MeatsMax;
        DairyRemaining = DairyMax;
        GrainsRemaining = GrainsMax;
        VegetablesRemaining = VegetablesMax;
        FruitsRemaining = FruitsMax;
        ThirstRemaining = ThirstMax;

        _firstPersonController = gameObject.GetComponent<FirstPersonController>();
        _characterController = gameObject.GetComponent<CharacterController>();
        _runSpeed = _firstPersonController.RunSpeed;

        ItemSelected = LocalInventory.Items[0];

        StartCoroutine(ProvideStartingItems());
    }

    public void Update() {
        // Attempting to remove blocks
        if (!IsViewingUI && !IsDraggingUI && ItemSelected == null && Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = new Ray(GlobalInfo.I.LocalCamera.gameObject.transform.position + GlobalInfo.I.LocalCamera.gameObject.transform.forward * 0.5f,
                              GlobalInfo.I.LocalCamera.gameObject.transform.forward * 3);
            if (Physics.Raycast(ray, out hit)) {
                SelectedBlockObj = hit.transform.gameObject;
            }
            _blockRemoveStartTime = Time.time;
        } else if (!IsViewingUI && !IsDraggingUI && ItemSelected == null && Input.GetMouseButton(0)) {
            if (SelectedBlockObj != null && SelectedBlockObj.GetComponent<BlockObject>() != null && _blockRemoveStartTime > 0.0f
                && (Time.time - _blockRemoveStartTime) > SelectedBlockObj.GetComponent<BlockObject>().BlockInfo.TimeToRemove) {
                BlockManager.I.TryRemoveBlock(SelectedBlockObj);
                SelectedBlockObj = null;
            }
        }

        // If using item
        if (!IsViewingUI && !IsDraggingUI && ItemSelected != null && Input.GetMouseButtonDown(0)) {
            _itemUseStartTime = Time.time;
        } else if (!IsViewingUI && !IsDraggingUI && ItemSelected != null && Input.GetMouseButton(0)) {
            if (_itemUseStartTime > 0.0f && (Time.time - _itemUseStartTime) > ItemSelected.TimeToUse) {
                UseItem(ItemSelected);
                _itemUseStartTime = Time.time;
            }
        }

        // If running
        if (!_firstPersonController.IsWalking) {
            EnergyRemaining -= EnergyToRun;
            _energyDelayTimer = Time.time + EnergyRegenDelay;
            if (EnergyRemaining <= 0) {
                EnergyRemaining = 0;
                _firstPersonController.RunSpeed = _firstPersonController.WalkSpeed;
            }
        } else if (Time.time > _energyDelayTimer) {
            EnergyRemaining = Mathf.Min(EnergyRegen + EnergyRemaining, EnergyMax);
            _firstPersonController.RunSpeed = _runSpeed;
        }

        // If jumping 
        if (_firstPersonController.AllowJump && _firstPersonController.Jumping) {
            _firstPersonController.AllowJump = false;
            EnergyRemaining -= EnergyToJump;
        } else if (_characterController.isGrounded && EnergyRemaining >= EnergyToJump) {
            _firstPersonController.AllowJump = true;
        }

        UIManager.I.UpdateEnergyUI();
        UIManager.I.UpdateHealthUI();
        if (Clock.I.TotalMinutes - _previousNutritionCheck > UpdateNutritionDelay) {
            UpdateNutrition();
            UIManager.I.TryUpdateUI();
        }
    }

    public IEnumerator ProvideStartingItems() {
        while (!UIManager.I.Initialized) {
            yield return null;
        }
        LocalInventory.AddItem(1, 30);
        LocalInventory.AddItem(7, 5);
        LocalInventory.AddItem(4, 8);
        LocalInventory.AddItem(53, 1);
        LocalInventory.AddItem(97, 1);
        LocalInventory.AddItem(151, 1);
        LocalInventory.AddItem(209, 1);
        LocalInventory.AddItem(98, 1);
    }

    public void UpdateNutrition() {
            HungerRemaining = HungerMax - (HungerRate * Clock.I.TotalMinutes);
            ThirstRemaining = ThirstMax - (ThirstRate * Clock.I.TotalMinutes);
            MeatsRemaining = MeatsMax - (MeatsRate * Clock.I.TotalMinutes);
            DairyRemaining = DairyMax - (DairyRate * Clock.I.TotalMinutes);
            GrainsRemaining = GrainsMax - (GrainsRate * Clock.I.TotalMinutes);
            VegetablesRemaining = VegetablesMax - (VegetablesRate * Clock.I.TotalMinutes);
            FruitsRemaining = FruitsMax - (FruitsRate * Clock.I.TotalMinutes);

            //UpdateMaxEnergy();
            UpdateEnergyRegen();
            UpdateMaxHealth();

            _previousNutritionCheck = Time.time;
    }

    public void UpdateMaxEnergy() {
        EnergyMax = Mathf.FloorToInt((Mathf.Min(HungerRemaining, HungerMax) + Mathf.Min(ThirstRemaining, ThirstMax)) * 0.5f);
        EnergyRemaining = Mathf.Min(EnergyRemaining, EnergyMax);
    }

    public void UpdateEnergyRegen() {
        EnergyRegen = 0.0005f * ((Mathf.Min(HungerRemaining, HungerMax) + Mathf.Min(ThirstRemaining, ThirstMax)) * 0.5f);
    }

    public void UpdateMaxHealth() {
        HealthMax = Mathf.FloorToInt((Mathf.Min(MeatsRemaining, MeatsMax) + Mathf.Min(DairyRemaining, DairyMax) + Mathf.Min(GrainsRemaining, GrainsMax)
                            + Mathf.Min(VegetablesRemaining, VegetablesMax) + Mathf.Min(FruitsRemaining, FruitsMax)) * 0.2f);
        HealthRemaining = Mathf.Min(HealthRemaining, HealthMax);
    }

    public void UseItem(Item item) {
        if (item.IsConsumable) {
            UseConsumable(item);
        }
        if (item.BlockInfo != null) {
            if (Time.time > (_timeOfLastPlacementAttempt + BlockManager.I.PlacementDelay)) {
                RaycastHit hit;
                Ray ray = new Ray(GlobalInfo.I.LocalCamera.gameObject.transform.position + GlobalInfo.I.LocalCamera.gameObject.transform.forward * 0.5f,
                                  GlobalInfo.I.LocalCamera.gameObject.transform.forward * BlockManager.I.PlacementDistance);
                //Debug.DrawRay(GlobalInfo.I.LocalCamera.gameObject.transform.position + GlobalInfo.I.LocalCamera.gameObject.transform.forward * 0.5f,
                //              GlobalInfo.I.LocalCamera.gameObject.transform.forward * PlacementManager.I.PlacementDistance, Color.red, 5, true);
                if (Physics.Raycast(ray, out hit)) {
                    if (Vector3.Distance(hit.point, transform.position) <= BlockManager.I.PlacementDistance) {
                        if (!BlockManager.I.TryPlaceBlock(new Vector3(hit.point.x, hit.point.y, hit.point.z), item.Id)) {
                            Vector3 onBlockFace = new Vector3(hit.point.x + (GlobalInfo.I.LocalCamera.gameObject.transform.forward.x * -0.01f),
                                                              hit.point.y + (GlobalInfo.I.LocalCamera.gameObject.transform.forward.y * -0.01f),
                                                              hit.point.z + (GlobalInfo.I.LocalCamera.gameObject.transform.forward.z * -0.01f));
                            BlockManager.I.TryPlaceBlock(onBlockFace, item.Id);
                        }
                    }
                }
                _timeOfLastPlacementAttempt = Time.time;
            }
        }
    }

    public void UseConsumable(Item consumable) {
        Effect consumeEffect = consumable.ConsumeEffect;
        int consumeAmount = Mathf.Min(consumable.Quantity, consumable.MaxConsumableStack);

        HungerRemaining = Mathf.Min(HungerRemaining + (consumeEffect.HungerRecover * consumeAmount), HungerMax);
        ThirstRemaining = Mathf.Min(ThirstRemaining + (consumeEffect.ThirstRecover * consumeAmount), ThirstMax);
        HealthRemaining = Mathf.Min(HealthRemaining + (consumeEffect.HealthRecover * consumeAmount), HealthMax);
        EnergyRemaining = Mathf.Min(EnergyRemaining + (consumeEffect.EnergyRecover * consumeAmount), EnergyMax);
        MeatsRemaining = Mathf.Min(MeatsRemaining + (consumeEffect.MeatRecover * consumeAmount), MeatsMax);
        DairyRemaining = Mathf.Min(DairyRemaining + (consumeEffect.DairyRecover * consumeAmount), DairyMax);
        GrainsRemaining = Mathf.Min(GrainsRemaining + (consumeEffect.GrainRecover * consumeAmount), GrainsMax);
        VegetablesRemaining = Mathf.Min(VegetablesRemaining + (consumeEffect.VegetableRecover * consumeAmount), VegetablesMax);
        FruitsRemaining = Mathf.Min(FruitsRemaining + (consumeEffect.FruitRecover * consumeAmount), FruitsMax);
        UIManager.I.UpdateNutritionUI();

        LocalInventory.RemoveItem(UIManager.I.InvUISlotSelectedIndex, consumeAmount);
    }

    public void ToggleCursor() {
        if (IsViewingUI) {
            _firstPersonController.GetMouseLook.SetCursorLock(false);
            UIManager.I.Crosshair.SetActive(false);
        } else {
            _firstPersonController.GetMouseLook.SetCursorLock(true);
            UIManager.I.Crosshair.SetActive(true);
        }
    }

    public void UpdateItemSelected() {
        ItemSelected = LocalInventory.Items[UIManager.I.InvUISlotSelectedIndex];
        UIManager.I.UpdateItemSelectedSprite();
    }
}
