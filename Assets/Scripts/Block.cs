﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
public class Block {

    public enum GeneratorType { None }

    public int Id;
    public bool IsPlaced;
    public Vector3 GridPosition; 
    public bool IsUpgradeable;
    public float TimeToRemove;

    public bool IsInventory;
    public int InventorySize;
    public Inventory MyInventory;

    public bool IsLiquidTank;
    public bool IsGasTank;
    public int LiquidTankSize;

    public GeneratorType IsGeneratorType;
    public bool IsGenerator;
    public int EnergyRateGenerated;
    public int EnergyRateConsumed;
    public int EnergyMax;
    public int EnergyCurrent;

    protected int _maxEnergyRateGenerated;

    public Block(int id, bool isPlaced, bool isUpgradeable, float timeToRemove, bool isInventory,
                 int inventorySize, Inventory inventory, bool isLiquidTank, bool isGasTank, int liquidTankSize,
                 bool isGenerator, int energyRateGenerated, int energyRateConsumed, int energyMax, int energyCurrent) {
        Id = id;
        IsPlaced = isPlaced;
        IsUpgradeable = isUpgradeable;
        TimeToRemove = timeToRemove;
        IsInventory = isInventory;
        InventorySize = inventorySize;
        MyInventory = inventory;
        IsLiquidTank = isLiquidTank;
        IsGasTank = isGasTank;
        LiquidTankSize = liquidTankSize;
        IsGenerator = isGenerator;
        EnergyRateGenerated = 0;
        EnergyRateConsumed = energyRateConsumed;
        EnergyMax = energyMax;
        EnergyCurrent = energyCurrent;

        _maxEnergyRateGenerated = energyRateGenerated;
    }
}
