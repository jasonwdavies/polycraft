﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer : MonoBehaviour {

    public Item PickupItem;
    public float LifeTime;
    public float AllowPickupDelay;
    public bool AllowPickup;
    public bool Dropped;

    private float _expireTime;
    private float _allowPickupTime;

    private void Start() {
        if (PickupItem != null) {
            gameObject.GetComponent<SpriteRenderer>().sprite = PickupItem.Icon;
        }

        _expireTime = Time.time + LifeTime;
        _allowPickupTime = Time.time + AllowPickupDelay;
        StartCoroutine(DelayExpire());
        if (Dropped) {
            StartCoroutine(DelayAllowPickup());
        } else {
            AllowPickup = true;
        }
    }

    private IEnumerator DelayExpire() {
        while (Time.time < _expireTime) {
            yield return new WaitForSeconds(1.0f); // TODO: global static variable that adjusts to slow this down when
        }
        Destroy(gameObject);
    }

    private IEnumerator DelayAllowPickup() {
        while (Time.time < _allowPickupTime) {
            yield return null;
        }
        AllowPickup = true;
    }
}
