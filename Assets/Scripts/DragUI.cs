﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public RectTransform ParentRT;
    public RectTransform MyRect;

    private bool _mouseDown = false;
    private Vector3 _startMousePos;
    private Vector3 _startPos;
    private bool _restrictX;
    private bool _restrictY;
    private float _fakeX;
    private float _fakeY;
    private float _myWidth;
    private float _myHeight;

    void Start() {
        _myWidth = (MyRect.rect.width + 5) / 2;
        _myHeight = (MyRect.rect.height + 5) / 2;
    }

    public void OnPointerDown(PointerEventData ped) {
        _mouseDown = true;
        _startPos = transform.position;
        _startMousePos = Input.mousePosition;
    }

    public void OnPointerUp(PointerEventData ped) {
        _mouseDown = false;
    }

    void Update() {
        if (_mouseDown) {
            Vector3 currentPos = Input.mousePosition;
            Vector3 diff = currentPos - _startMousePos;
            Vector3 pos = _startPos + diff;
            transform.position = pos;

            if (transform.localPosition.x < 0 - ((ParentRT.rect.width / 2) - _myWidth) || transform.localPosition.x > ((ParentRT.rect.width / 2) - _myWidth)) {
                _restrictX = true;
            } else {
                _restrictX = false;
            }

            if (transform.localPosition.y < 0 - ((ParentRT.rect.height / 2) - _myHeight) || transform.localPosition.y > ((ParentRT.rect.height / 2) - _myHeight)) {
                _restrictY = true;
            } else {
                _restrictY = false;
            }

            if (_restrictX) {
                if (transform.localPosition.x < 0)
                    _fakeX = 0 - (ParentRT.rect.width / 2) + _myWidth;
                else
                    _fakeX = (ParentRT.rect.width / 2) - _myWidth;

                Vector3 xpos = new Vector3(_fakeX, transform.localPosition.y, 0.0f);
                transform.localPosition = xpos;
            }

            if (_restrictY) {
                if (transform.localPosition.y < 0)
                    _fakeY = 0 - (ParentRT.rect.height / 2) + _myHeight;
                else
                    _fakeY = (ParentRT.rect.height / 2) - _myHeight;

                Vector3 ypos = new Vector3(transform.localPosition.x, _fakeY, 0.0f);
                transform.localPosition = ypos;
            }
        }
    }
}
