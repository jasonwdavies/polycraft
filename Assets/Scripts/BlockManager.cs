﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour
{

    private static BlockManager _I;
    public static BlockManager I
    {
        get
        {
            if (_I == null) {
                _I = FindObjectOfType<BlockManager>();
            }
            return _I;
        }
    }
    
    //public GameObject DebugSphere;

    [Header("Placement Data")]
    public float PlacementDelay;
    public float PlacementDistance;

    [Header("Grid Data")]
    public int Length;
    public int Width;
    public int Height;
    public GridSlot[,,] Grid;

    private void Start() {
        Grid = new GridSlot[Length, Width, Height];
        SetGridSlotPositions();
    }

    public void SetGridSlotPositions() {
        for (int i = 0; i < Length; i++) {
            for (int j = 0; j < Height; j++) {
                for (int k = 0; k < Width; k++) {
                    Grid[i, j, k] = new GridSlot(new Vector3(i, j, k), -1);
                }
            }
        }
    }

    public bool TryPlaceBlock(Vector3 position, int blockId) {
        //GameObject debugSphere = Instantiate(DebugSphere);
        //debugSphere.transform.position = position;

        Vector3 roundedPosition = new Vector3(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y), Mathf.RoundToInt(position.z));
        GridSlot slot = GetGridSlot(roundedPosition);

        if (slot != null && slot.OccupiedId == -1) {
            PlaceBlock(new Vector3(roundedPosition.x, roundedPosition.y, roundedPosition.z), blockId);
            return true;
        }
        return false;
    }

    public void PlaceBlock(Vector3 pos, int blockId) {
        GameObject blockObj = GlobalInfo.I.Block;

        switch (blockId) {
            case 97: // Chest
                blockObj = GlobalInfo.I.Chest;
                break;
            case 123: // Forge
                blockObj = GlobalInfo.I.Forge;
                break;
            case 124: // Firepit
                blockObj = GlobalInfo.I.Firepit;
                break;
            case 151: // Generator
                blockObj = GlobalInfo.I.Block;
                break;
        }

        blockObj = Instantiate(blockObj);
        blockObj.transform.position = pos;
        blockObj.GetComponent<BlockObject>().BlockInfo = ItemManager.I.Items[blockId].BlockInfo;
        blockObj.GetComponent<BlockObject>().BlockInfo.Id = GlobalInfo.I.LocalPlayer.ItemSelected.Id;
        blockObj.GetComponent<BlockObject>().BlockInfo.IsPlaced = true;
        blockObj.GetComponent<BlockObject>().BlockInfo.GridPosition = pos;
        GlobalInfo.I.LocalPlayer.LocalInventory.RemoveItem(GlobalInfo.I.LocalPlayer.ItemSelected, 1);
        Grid[(Length / 2) + (int)pos.x, (Height / 2) + (int)pos.y, (Width / 2) + (int)pos.z].OccupiedId = blockId;
        Debug.Log(blockObj.transform.position);
    }

    public void TryRemoveBlock(GameObject block) {
        RemoveBlock(block);
    }

    public void RemoveBlock(GameObject blockObj) {
        Vector3 pos = blockObj.transform.position;
        int occupiedId = Grid[(Length / 2) + (int)pos.x, (Height / 2) + (int)pos.y, (Width / 2) + (int)pos.z].OccupiedId;
        //Block block = blockObj.GetComponent<Block>();

        //Grid[Mathf.FloorToInt(pos.x), Mathf.FloorToInt(pos.y), Mathf.FloorToInt(pos.z)].OccupiedId = -1;
        GameObject itemContainer = Instantiate(GlobalInfo.I.ItemContainer);
        itemContainer.transform.position = blockObj.transform.position;
        itemContainer.GetComponent<ItemContainer>().PickupItem = ItemManager.I.Items[occupiedId];
        Grid[(Length / 2) + (int)pos.x, (Height / 2) + (int)pos.y, (Width / 2) + (int)pos.z].OccupiedId = -1;
        Destroy(blockObj);
    }

    public GridSlot GetGridSlot(Vector3 roundedPosition) {
        return Grid[(Length / 2) + (int)roundedPosition.x, (Height / 2) + (int)roundedPosition.y, (Width / 2) + (int)roundedPosition.z];
    }
}
