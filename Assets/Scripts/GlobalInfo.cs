﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInfo : MonoBehaviour {

    private static GlobalInfo _I;
    public static GlobalInfo I {
        get {
            if (_I == null) {
                _I = FindObjectOfType<GlobalInfo>();
            }
            return _I;
        }
    }

    [Header("Players")]
    public Player LocalPlayer; // set in the editor
    public Camera LocalCamera; // set in the editor

    public string[] Seasons = { "Winter", "LateWinter", "EarlySpring", "Spring", "LateSpring", "EarlySummer", "Summer", "LateSummer", "EarlyFall", "Fall", "LateFall", "EarlyWinter" };
    public int CurrentSeason { get { return (Clock.I.TotalMinutes / (60 * 24 * 30)) % 12; } }
    public enum Weather { Humid, Sunny, Windy, Rainy, Snowing, Cloudy };
    public Weather CurrentWeather;
    public float Temperature;
    public float TemperatureModifier = 1;
    public float ChillTemperature = 45;
    public float SunriseTime;
    public float SunsetTime;

    [Header("Useful Prefabs")]
    public GameObject Block;
    public GameObject Chest;
    public GameObject Firepit;
    public GameObject Forge;
    public GameObject ItemContainer;
    public GameObject BlackberryBush;
    public GameObject BlueberryBush;
    public GameObject CranberryBush;
    public GameObject StrawberryBush;
    public GameObject GreenPepperBush;
    public GameObject RedPepperBush;
    public GameObject YellowPepperBush;
    public GameObject PotatoeBush;

    public List<Inventory> InventoriesToUpdate = new List<Inventory>();
    public List<SmartBlock> SmartBlocksToUpdate = new List<SmartBlock>();

    private void Start() {
        CurrentWeather = Weather.Sunny;
        StartCoroutine(UpdateCycle());
    }

    private IEnumerator UpdateCycle() {
        while (true) {
            yield return new WaitForSeconds(1.0f);

            for (int i = 0; i < InventoriesToUpdate.Count; i++) {
                InventoriesToUpdate[i].UpdateItems();
            }

            for (int i = 0; i < SmartBlocksToUpdate.Count; i++) {
                if (SmartBlocksToUpdate[i].IsPlaced) {
                    SmartBlocksToUpdate[i].TryDelayedCrafting();
                    if (UIManager.I.SelectedBlock != null) {
                        switch (UIManager.I.SelectedBlock.BlockInfo.Id) {
                            case 97: // Chest
                                UIManager.I.UpdateSlotUI(UIManager.I.ChestSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                break;
                            case 123: // Forge
                                UIManager.I.UpdateSlotUI(UIManager.I.ForgeSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                break;
                            case 124: // Firepit
                                UIManager.I.UpdateSlotUI(UIManager.I.FirepitSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                break;
                            case 151: // Generator
                                UIManager.I.UpdateSlotUI(UIManager.I.GeneratorSlots, UIManager.I.SelectedBlock.BlockInfo.MyInventory);
                                break;
                        }
                    }
                }
            }
        }
    }

    public void CalculateTemperatureModifier() { }
}
