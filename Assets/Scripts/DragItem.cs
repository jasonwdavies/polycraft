﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// Reponsible for transferring data between UIItems
/// </summary>
public class DragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler {

    public UIItem MouseSlotItem;
    public int SlotNumber;

    public enum SlotType { PlayerInventory, Recipe, SmartBlock }
    public SlotType Type;

    private Color _mouseSlotItemImageColor;
    private Color _mouseSlotItemTextColor;
    private GameObject _releasedOnSlot;

    private void Update() {
        if (UIManager.I.TooltipUI.activeInHierarchy && EventSystem.current.IsPointerOverGameObject()) {
            UIManager.I.TooltipUI.transform.position = Input.mousePosition;
        }
    }
    
    public void OnPointerEnter(PointerEventData eventData) {
        Item item = transform.GetChild(0).GetComponent<UIItem>().MyItem;
        if (item != null) {
            UIManager.I.TooltipUI.SetActive(true);
            UIManager.I.TooltipText.text = "Name: " + item.Name + "\nID: " + item.Id;
            if (Type == SlotType.Recipe) {
                Crafting.I.SetCurrentRecipe(SlotNumber);
                UIManager.I.TooltipText.text += "\nIngredients: ";
                Ingredient[] ingredients = Crafting.I.CurrentRecipe.Ingredients;
                for (int i=0; i<ingredients.Length; i++) {
                    if (ingredients[i].Quantity != 0) {
                        UIManager.I.TooltipText.text += "\n- " + ItemManager.I.GetItem(ingredients[i].ItemID).Name  + " (" 
                                                        + GlobalInfo.I.LocalPlayer.LocalInventory.GetItemQuantity(ItemManager.I.GetItem(ingredients[i].ItemID))
                                                        + "/" + ingredients[i].Quantity + ")";
                    }
                }
                switch (Crafting.I.CurrentRecipe.CraftingMethod) {
                    case Crafting.CraftingMethod.Firepit:
                        UIManager.I.TooltipText.text += "\nRequires: Firepit interaction";
                        break;
                    case Crafting.CraftingMethod.Anvil:
                        UIManager.I.TooltipText.text += "\nRequires: Anvil interaction";
                        break;
                    case Crafting.CraftingMethod.Forge:
                        UIManager.I.TooltipText.text += "\nRequires: Forge interaction";
                        break;
                    case Crafting.CraftingMethod.Woodwork:
                        UIManager.I.TooltipText.text += "\nRequires: X interaction";
                        break;
                }
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        UIManager.I.TooltipUI.SetActive(false);
    }

    public void OnBeginDrag(PointerEventData eventData) {
        UIItem itemDragged = transform.GetChild(0).gameObject.GetComponent<UIItem>();
        GlobalInfo.I.LocalPlayer.IsDraggingUI = true;
        UIManager.I.TooltipUI.SetActive(false);

        if (Type == SlotType.Recipe && Crafting.I.SetCurrentRecipe(SlotNumber) && Crafting.I.CurrentRecipe.IsCraftable && itemDragged.MyItem != null) {
            MouseSlotItem.CopyUIItem(itemDragged);
        } else if (Type == SlotType.SmartBlock && itemDragged.MyItem != null) {
            MouseSlotItem.CopyUIItem(itemDragged);
            itemDragged.ClearUIItem();
            UIManager.I.SelectedBlock.BlockInfo.MyInventory.Items[SlotNumber] = null;
        } else if (!(Type == SlotType.Recipe) && !(Type == SlotType.SmartBlock) && itemDragged.MyItem != null) {
            MouseSlotItem.SwapUIItem(itemDragged);
            GlobalInfo.I.LocalPlayer.LocalInventory.Items[SlotNumber] = null;
        }
        UIManager.I.UpdateCraftingUI();
        UIManager.I.UpdateInventoryUI();
        UIManager.I.TryUpdateChestUI();
        GlobalInfo.I.LocalPlayer.UpdateItemSelected();
    }

    public void OnDrag(PointerEventData eventData) {
        MouseSlotItem.transform.position = Input.mousePosition;
        _releasedOnSlot = eventData.pointerCurrentRaycast.gameObject;
    }

    public void OnEndDrag(PointerEventData eventData) {
        UIItem releasedOnItem;

        if (Type == SlotType.Recipe && Crafting.I.CurrentRecipe.IsCraftable && UIManager.I.SelectedBlock != null) {
            Crafting.I.UseRecipeIngredients(UIManager.I.SelectedBlock.BlockInfo.MyInventory);
        } else if (Type == SlotType.Recipe && Crafting.I.CurrentRecipe.IsCraftable && UIManager.I.SelectedBlock == null) {
            Crafting.I.UseRecipeIngredients(GlobalInfo.I.LocalPlayer.LocalInventory);
        }

        if (_releasedOnSlot != null && _releasedOnSlot.transform.childCount > 0 && _releasedOnSlot.transform.GetChild(0).GetComponent<UIItem>()) { // Released on a valid slot
            releasedOnItem = _releasedOnSlot.transform.GetChild(0).GetComponent<UIItem>();
            if (releasedOnItem.MyItem == null) { // empty slot
                releasedOnItem.CopyUIItem(MouseSlotItem);
                MouseSlotItem.ClearUIItem();
                SlotType releasedOnSlotType = releasedOnItem.transform.parent.GetComponent<DragItem>().Type;

                if (releasedOnSlotType == SlotType.SmartBlock) { // if we dragged onto an open crafting block slot
                    UIManager.I.SelectedBlock.BlockInfo.MyInventory.Items[_releasedOnSlot.GetComponent<DragItem>().SlotNumber] = releasedOnItem.MyItem;
                    if (UIManager.I.SelectedBlock.BlockInfo is SmartBlock) {
                        ((SmartBlock)(UIManager.I.SelectedBlock.BlockInfo)).TryDelayedCrafting();
                    }
                } else { // dragged onto player inventory
                    GlobalInfo.I.LocalPlayer.LocalInventory.Items[_releasedOnSlot.GetComponent<DragItem>().SlotNumber] = releasedOnItem.MyItem;
                }
            } else { // occupied slot
                MouseSlotItem.DropUIItem();
                if (!(Type == SlotType.Recipe)) {
                    GlobalInfo.I.LocalPlayer.LocalInventory.Items[SlotNumber] = null;
                }
            }
        } else { // Released on an invalid area
            if (MouseSlotItem.MyItem != null) {
                MouseSlotItem.DropUIItem();
                if (!(Type == SlotType.Recipe)) {
                    GlobalInfo.I.LocalPlayer.LocalInventory.Items[SlotNumber] = null;
                }
            }
        }
        UIManager.I.UpdateCraftingUI();
        UIManager.I.UpdateInventoryUI();
        UIManager.I.TryUpdateChestUI();
        GlobalInfo.I.LocalPlayer.UpdateItemSelected();
        GlobalInfo.I.LocalPlayer.IsDraggingUI = false;
    }
}
