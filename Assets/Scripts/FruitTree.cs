﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitTree : Tree {

    public Item Fruit; // set in editor
    public int[] FruitSeason;
    public int FruitMinCount;
    public int FruitMaxCount;
    public int FruitCount;
    public int ChillHours; // Hours needed to be below GlobalInfo.I.ChillTemperature
    public bool Citrus; // If true, reduces nearby tree's FruitMaxCount

    private new void Start() {
        //Fruit = new Consumable(Fruit.DecayRate, Fruit.LifeSpan, Fruit.Name, Fruit.Icon, Fruit.Weight, Fruit.Value);
        base.Start(); // handles growing of the tree
    }

    private new void Update() {
        if (_mature && FruitCount == 0 && (Clock.I.TotalMinutes / (60 * 24)) > _lastDayChecked) {
            for (int i=0; i<FruitSeason.Length; i++) {
                if (FruitSeason[i] == GlobalInfo.I.CurrentSeason) {
                    GrowFruit();
                    break;
                }
            }
            _lastDayChecked = Clock.I.TotalMinutes / (60 * 24);
        }
        base.Update();
    }

    private void GrowFruit() {
        FruitCount = Random.Range(FruitMinCount, FruitMaxCount);
    }

    public void HarvestFruit() {
        if (FruitCount > 0) {
            Item fruit = new Item(Fruit);
            ItemContainer itemContainer = Instantiate(GlobalInfo.I.ItemContainer).GetComponent<ItemContainer>();

            itemContainer.gameObject.transform.position = transform.position + new Vector3(1, 2, 1);
            itemContainer.PickupItem = fruit;
            itemContainer.PickupItem.Quantity = FruitCount;
            FruitCount = 0;
        }
    }
}