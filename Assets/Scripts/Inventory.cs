﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Inventory {

    public int Capacity;
    public int TotalWeight;
    public Item[] Items;

    public Inventory(int capacity) {
        Capacity = capacity;

        Items = new Item[Capacity];
        GlobalInfo.I.InventoriesToUpdate.Add(this);
    }

    public bool AddItem(int id, int quantity, bool updateUI = true) {
        for (int i = 0; i < Capacity; i++) {
            if (AddItem(id, quantity, i, updateUI)) {
                return true;
            }
        }
        return false;
    }

    public bool AddItem(int id, int quantity, int index, bool updateUI = true) {
        Item item = ItemManager.I.Items[id];

        if (Items[index] == null) {
            item.Quantity = quantity;
            Items[index] = item;
            TotalWeight += quantity;
            if (updateUI) {
                UIManager.I.UpdateInventoryUI();
                UIManager.I.UpdateCraftingUI();
                UIManager.I.TryUpdateChestUI();
                GlobalInfo.I.LocalPlayer.UpdateItemSelected();
            }
            return true;
        }
        else if (Items[index].Name.Equals(item.Name)) {
            Items[index].Quantity += quantity;
            TotalWeight++;
            if (updateUI) {
                UIManager.I.UpdateInventoryUI();
                UIManager.I.UpdateCraftingUI();
                UIManager.I.TryUpdateChestUI();
                GlobalInfo.I.LocalPlayer.UpdateItemSelected();
            }
            return true;
        }
        return false;
    }

    public bool AddItem(Item item, bool updateUI = true) {
        for (int i=0; i<Capacity; i++) {
            if (Items[i] == null) {
                Items[i] = item;
                TotalWeight += item.Quantity;
                if (updateUI) {
                    UIManager.I.UpdateInventoryUI();
                    UIManager.I.UpdateCraftingUI();
                    UIManager.I.TryUpdateChestUI();
                    GlobalInfo.I.LocalPlayer.UpdateItemSelected();
                }
                return true;
            } else if (Items[i] != null && Items[i].Name.Equals(item.Name)) {
                Items[i].Quantity += item.Quantity;
                TotalWeight += item.Quantity;
                if (updateUI) {
                    UIManager.I.UpdateInventoryUI();
                    UIManager.I.UpdateCraftingUI();
                    UIManager.I.TryUpdateChestUI();
                    GlobalInfo.I.LocalPlayer.UpdateItemSelected();
                }
                return true;
            }
        }
        return false;
    }

    public bool RemoveItem(Item item, bool updateUI = true) {
        for (int i = 0; i < Capacity; i++) {
            if (Items[i] != null && Items[i].Name.Equals(item.Name)) {
                Items[i].Quantity -= item.Quantity;
                TotalWeight -= item.Quantity;
                if (Items[i].Quantity == 0) {
                    Items[i] = null;
                }
                if (updateUI) {
                    UIManager.I.UpdateInventoryUI();
                    UIManager.I.UpdateCraftingUI();
                    UIManager.I.TryUpdateChestUI();
                    GlobalInfo.I.LocalPlayer.UpdateItemSelected();
                }
                return true;
            }
        }
        return false;
    }

    public bool RemoveItem(Item item, int quantity, bool updateUI = true) {
        for (int i = 0; i < Capacity; i++) {
            if (Items[i] != null && Items[i].Name.Equals(item.Name) && Items[i].Quantity >= quantity) {
                Items[i].Quantity -= quantity;
                TotalWeight -= quantity;
                if (Items[i].Quantity == 0) {
                    Items[i] = null;
                }
                if (updateUI) {
                    UIManager.I.UpdateInventoryUI();
                    UIManager.I.UpdateCraftingUI();
                    UIManager.I.TryUpdateChestUI();
                    GlobalInfo.I.LocalPlayer.UpdateItemSelected();
                }
                return true;
            }
        }
        return false;
    }

    public bool RemoveItem(int slotId, int quantity, bool updateUI = true) {
        if (Items[slotId] != null && Items[slotId].Quantity >= quantity) {
            Items[slotId].Quantity -= quantity;
            TotalWeight -= quantity;
            if (Items[slotId].Quantity == 0) {
                Items[slotId] = null;
            }
            if (updateUI) {
                UIManager.I.UpdateInventoryUI();
                UIManager.I.UpdateCraftingUI();
                UIManager.I.TryUpdateChestUI();
                GlobalInfo.I.LocalPlayer.UpdateItemSelected();
            }
            return true;
        }
        return false;
    }

    public int GetItemQuantity(Item item) {
        int tally = 0;

        for (int i = 0; i < Items.Length; i++) {
            if (Items[i] != null && Items[i].IsOfCategory(item.IsKeyIngredientType)) {
                if (item.IsKeyIngredientType == Item.KeyIngredient.None && item.Equals(Items[i])) {
                    tally += Items[i].Quantity;
                }
                else if (item.IsKeyIngredientType != Item.KeyIngredient.None) {
                    tally += Items[i].Quantity;
                }
            }
        }
        return tally;
    }

    public List<int> GetItemLocations(Item item, int quantity) {
        List<int> locations = new List<int>();
        int tally = 0;

        for (int i=0; i<Items.Length; i++) {
            if (Items[i] != null && item != null && Items[i].IsOfCategory(item.IsKeyIngredientType)) {
                if (item.IsKeyIngredientType == Item.KeyIngredient.None && item.Equals(Items[i])) { // none
                    tally += Items[i].Quantity;
                    locations.Add(i);
                } else if (item.IsKeyIngredientType != Item.KeyIngredient.None) {
                    tally += Items[i].Quantity;
                    locations.Add(i);
                }
            }
            if (tally >= quantity) {
                return locations;
            }
        }
        return null;
    }

    public List<int> GetItemLocations(Item item, int quantity, List<int> constraintIndices) {
        List<int> locations = new List<int>();
        int tally = 0;

        for (int i = 0; i < Items.Length; i++) {
            if (constraintIndices.Contains(i) && Items[i] != null && item != null && Items[i].IsOfCategory(item.IsKeyIngredientType)) {
                if (item.IsKeyIngredientType == Item.KeyIngredient.None && item.Equals(Items[i])) { // none
                    tally += Items[i].Quantity;
                    locations.Add(i);
                }
                else if (item.IsKeyIngredientType != Item.KeyIngredient.None) {
                    tally += Items[i].Quantity;
                    locations.Add(i);
                }
            }
            if (tally >= quantity) {
                return locations;
            }
        }
        return null;
    }

    public void UpdateItems() {
        for (int i=0; i<Items.Length; i++) {
            if (Items[i] != null && Items[i].IsConsumable) {
                Items[i].Decay();
                if ((Items[i]).IsExpired() || Items[i].Quantity <= 0) {
                    Items[i] = null;
                    GlobalInfo.I.LocalPlayer.UpdateItemSelected();
                }
            }
        }
    }
}
