﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour {

    public float TimeCreated;
    public float GrowthRate;
    public float GrowthRemaining;
    public float MatureValue;

    protected bool _mature;
    protected int _lastDayChecked;

    protected void Start() {
        TimeCreated = Clock.I.TotalMinutes;
        GrowthRemaining = MatureValue;
    }

    protected void Update() {
        if (GrowthRemaining > 0) {
            Grow();
        }   
    }

    public void Grow() {
        GrowthRemaining = MatureValue - (Clock.I.TotalMinutes - TimeCreated);
        if (GrowthRemaining < 0) {
            GrowthRemaining = 0;
        }
        ScaleUp();
    }

    public void ScaleUp() {
        float val = GrowthRate * (MatureValue - GrowthRemaining);
        gameObject.transform.localScale = new Vector3(val, val, val);
    }
}
