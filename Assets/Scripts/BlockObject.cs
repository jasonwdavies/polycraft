﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockObject : MonoBehaviour {

    public Block BlockInfo;
    public bool PlayedAnimation;

    private void Start() {
        gameObject.name = ItemManager.I.Items[BlockInfo.Id].Name;
        PlayedAnimation = true;
    }

    public void TryPlayAnimation() {
        if (!PlayedAnimation) {
            PlayedAnimation = true;
            switch (BlockInfo.Id) {
                case 97:
                    // Open lid
                    break;
                case 123: // Forge
                case 124: // Firepit
                    transform.GetChild(0).gameObject.SetActive(true);
                    break;
            }
        }
    }

    public void TryResetAnimation() {
        if (PlayedAnimation) {
            PlayedAnimation = false;
            switch (BlockInfo.Id) {
                case 97:
                    // Open lid
                    break;
                case 123: // Forge
                case 124: // Firepit
                    transform.GetChild(0).gameObject.SetActive(false);
                    break;
            }
        }
    }

    private void Update() {
        if (BlockInfo is SmartBlock && ((SmartBlock)BlockInfo).ValidRecipe != null) {
            TryPlayAnimation();
        } else if (BlockInfo is SmartBlock && ((SmartBlock)BlockInfo).ValidRecipe == null) {
            TryResetAnimation();
        }
    }
}
