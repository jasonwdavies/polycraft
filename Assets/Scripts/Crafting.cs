﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crafting : MonoBehaviour {

    private static Crafting _I;
    public static Crafting I {
        get {
            if (_I == null) {
                _I = FindObjectOfType<Crafting>();
            }
            return _I;
        }
    }

    public enum CraftingMethod { Other, Firepit, Anvil, Forge, Woodwork, Generator }

    public Dictionary<CraftingMethod, Recipe[]> AllRecipes; // int - CraftingCategory

    [Header("Recipe Data")]
    public Recipe[] OtherRecipes;
    public Recipe[] FirepitRecipes;
    public Recipe[] AnvilRecipes;
    public Recipe[] ForgeRecipes;
    public Recipe[] WoodworkRecipes;
    public Recipe[] GeneratorRecipes;

    [Header("Flags")]
    public bool AllowFirepitCrafting;
    public bool AllowAnvilCrafting;
    public bool AllowForgeCrafting;
    public bool AllowWoodworkCrafting;
    public bool HasWood;
    public bool HasStone;
    public bool HasCopper;

    private Recipe _currentRecipe;
    public Recipe CurrentRecipe { get { return _currentRecipe; } set { _currentRecipe = value; } }
    private CraftingMethod _methodSelected;
    public CraftingMethod MethodSelected { get { return _methodSelected; } set { _methodSelected = value; } }

    public void Start() {
        // Add them to AllRecipes
        AllRecipes = new Dictionary<CraftingMethod, Recipe[]>();
        AllRecipes.Add(CraftingMethod.Other, OtherRecipes);
        AllRecipes.Add(CraftingMethod.Firepit, FirepitRecipes);
        AllRecipes.Add(CraftingMethod.Anvil, AnvilRecipes);
        AllRecipes.Add(CraftingMethod.Forge, ForgeRecipes);
        AllRecipes.Add(CraftingMethod.Woodwork, WoodworkRecipes);
        AllRecipes.Add(CraftingMethod.Generator, GeneratorRecipes);
    }

    public void CheckForKeyIngredients(Inventory inventory) {
        Item[] items = inventory.Items;
        HasWood = false;
        HasStone = false;
        HasCopper = false;

        for (int i=0; i<items.Length; i++) {
            if (!HasWood && items[i].IsOfCategory(Item.KeyIngredient.Wood)) {
                HasWood = true;
            } else if (!HasStone && items[i].IsOfCategory(Item.KeyIngredient.Stone)) {
                HasStone = true;
            } else if (!HasCopper && items[i].IsOfCategory(Item.KeyIngredient.Copper)) {
                HasCopper = true;
            }
        }
    }

    public bool CheckForIngredients(Recipe recipe, Inventory inventory, List<int> constraintIndices = null) {
        List<int> haveIngredient = null;

        for (int i = 0; i < recipe.Ingredients.Length; i++) {
            if (constraintIndices == null && recipe.Ingredients[i].Quantity > 0) {
                haveIngredient = inventory.GetItemLocations(ItemManager.I.Items[recipe.Ingredients[i].ItemID], recipe.Ingredients[i].Quantity);
            } else if (recipe.Ingredients[i].Quantity > 0) {
                haveIngredient = inventory.GetItemLocations(ItemManager.I.Items[recipe.Ingredients[i].ItemID], recipe.Ingredients[i].Quantity, constraintIndices);
            }

            if (haveIngredient == null) {
                return false;
            }
        }
        return true;
    }

    public void UseRecipeIngredients(Inventory inventory) {
        List<int> ingredientSlots = null;
        int totalWeight;
        int removeWeight;

        // Remove Ingredients from Inventory
        for (int i = 0; i < CurrentRecipe.Ingredients.Length; i++) {
            if (CurrentRecipe.Ingredients[i].Quantity > 0) {
                totalWeight = CurrentRecipe.Ingredients[i].Quantity;
                ingredientSlots = inventory.GetItemLocations(ItemManager.I.Items[CurrentRecipe.Ingredients[i].ItemID], CurrentRecipe.Ingredients[i].Quantity);
                for (int j = 0; j < ingredientSlots.Count; j++) {
                    removeWeight = Mathf.Min(inventory.Items[ingredientSlots[j]].Quantity, totalWeight);
                    inventory.RemoveItem(ingredientSlots[j], removeWeight);
                    totalWeight -= removeWeight;
                    if (totalWeight == 0) {
                        break;
                    }
                }
            }
        }
    }

    public void UseRecipeIngredients(Recipe recipe, Inventory inventory) {
        List<int> ingredientSlots = null;
        int totalWeight;
        int removeWeight;

        // Remove Ingredients from Inventory
        for (int i = 0; i < recipe.Ingredients.Length; i++) {
            totalWeight = recipe.Ingredients[i].Quantity;
            ingredientSlots = inventory.GetItemLocations(ItemManager.I.Items[recipe.Ingredients[i].ItemID], recipe.Ingredients[i].Quantity);
            for (int j = 0; j < ingredientSlots.Count; j++) {
                removeWeight = Mathf.Min(inventory.Items[ingredientSlots[j]].Quantity, totalWeight);
                inventory.RemoveItem(ingredientSlots[j], removeWeight);
                totalWeight -= removeWeight;
                if (totalWeight == 0) {
                    break;
                }
            }
        }
    }

    public bool SetCurrentRecipe(int index) {
        switch (MethodSelected) {
            case CraftingMethod.Other:
                CurrentRecipe = OtherRecipes[index];
                return true;
            case CraftingMethod.Firepit:
                CurrentRecipe = FirepitRecipes[index];
                return true;
            case CraftingMethod.Anvil:
                CurrentRecipe = AnvilRecipes[index];
                return true;
            case CraftingMethod.Forge:
                CurrentRecipe = ForgeRecipes[index];
                return true;
            case CraftingMethod.Woodwork:
                CurrentRecipe = WoodworkRecipes[index];
                return true;
            case CraftingMethod.Generator:
                CurrentRecipe = GeneratorRecipes[index];
                break;
        }
        return false;
    }
}
