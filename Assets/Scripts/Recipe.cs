﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Recipe {

    public int Id;
    public string Name;
    public Crafting.CraftingMethod CraftingMethod;
    public Item.KeyIngredient KeyIngredient;
    public int ResultID;
    public Ingredient[] Ingredients; // used to fill IngredientQuantities
    public float TimeToCraft;

    public bool IsCraftable { get { return _isCraftable; } set { _isCraftable = value; } }
    private bool _isCraftable;

    public Recipe(int id, string name, Crafting.CraftingMethod craftingMethod, Item.KeyIngredient keyIngredient, 
                  int resultID, Ingredient[] ingredients, float timeToCraft) {
        Id = id;
        Name = name;
        CraftingMethod = craftingMethod;
        KeyIngredient = keyIngredient;
        ResultID = resultID;
        Ingredients = ingredients;
        TimeToCraft = timeToCraft;
    }
}
