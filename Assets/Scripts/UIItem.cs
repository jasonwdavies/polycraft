﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// The representation of the slot and the parts you see in it
/// </summary>
public class UIItem : MonoBehaviour {

    public Image Icon;
    public Text Quantity;
    public Image Bar;
    public Image BarBackground;

    public Item MyItem { get { return _myItem; } set { _myItem = value; } }

    private Item _myItem;

    public void SetCraftingUIItem(Item item) {
        Icon.sprite = item.Icon;
        Icon.color = new Color(Icon.color.r, Icon.color.g, Icon.color.b, 255);
        _myItem = item;
    }

    /// <summary>
    /// Copies data from uiItem input to this UIItem and its Item
    /// </summary>
    /// <param name="uiItem"></param>
    public void CopyUIItem(UIItem uiItem) {
        Icon.sprite = uiItem.Icon.sprite;
        Icon.color = new Color(Icon.color.r, Icon.color.g, Icon.color.b, 255);
        Quantity.text = uiItem.Quantity.text;
        Quantity.color = new Color(Quantity.color.r, Quantity.color.g, Quantity.color.b, 255);
        Bar.color = new Color(Bar.color.r, Bar.color.g, Bar.color.b, 255);
        BarBackground.color = new Color(BarBackground.color.r, BarBackground.color.g, BarBackground.color.b, 255);
        _myItem = uiItem._myItem;
    }

    /// <summary>
    /// Swap data between two UIItems and their Items
    /// </summary>
    /// <param name="uiItem"></param>
    public void SwapUIItem(UIItem uiItem) {
        CopyUIItem(uiItem);
       
        uiItem.Icon.sprite = null;
        uiItem.Icon.color = new Color(uiItem.Icon.color.r, uiItem.Icon.color.g, uiItem.Icon.color.b, 0);
        uiItem.Quantity.text = "";
        uiItem.Quantity.color = new Color(uiItem.Quantity.color.r, uiItem.Quantity.color.g, uiItem.Quantity.color.b, 0);
        uiItem.Bar.color = new Color(Bar.color.r, Bar.color.g, Bar.color.b, 0);
        uiItem.BarBackground.color = new Color(BarBackground.color.r, BarBackground.color.g, BarBackground.color.b, 0);
        uiItem._myItem = null;
    }

    /// <summary>
    /// Drops the Item and clears the UIItem data
    /// </summary>
    public void DropUIItem() {
        GameObject itemContainerObj = Instantiate(GlobalInfo.I.ItemContainer);
        ItemContainer itemContainer = itemContainerObj.GetComponent<ItemContainer>();
        //Rigidbody itemContainerRB;
        //Vector3 force;

        itemContainer.AllowPickupDelay = 1.0f;
        itemContainer.Dropped = true;
        itemContainer.PickupItem = _myItem;
        itemContainer.PickupItem.Quantity = _myItem.Quantity;
        itemContainerObj.GetComponent<SpriteRenderer>().sprite = Icon.sprite;
        itemContainerObj.transform.position = GlobalInfo.I.LocalCamera.gameObject.transform.position + GlobalInfo.I.LocalCamera.gameObject.transform.forward * 2;
        ClearUIItem();

        //itemContainerRB = itemContainerObj.GetComponent<Rigidbody>();
        //if (itemContainerRB != null) {
        //    force = new Vector3(GlobalInfo.I.LocalPlayer.gameObject.transform.rotation.x,
        //                        GlobalInfo.I.LocalPlayer.gameObject.transform.rotation.y, 
        //                        GlobalInfo.I.LocalPlayer.gameObject.transform.rotation.z);
        //    itemContainerRB.AddForce((force.normalized)*10);
        //}
    }

    /// <summary>
    /// Clears the UIItem of all data
    /// </summary>
    public void ClearUIItem() {
        //itemContainerObj.GetComponent<SpriteRenderer>().sprite = Icon.sprite;
        Icon.sprite = null;
        Icon.color = new Color(Icon.color.r, Icon.color.g, Icon.color.b, 0);
        Quantity.text = "";
        Quantity.color = new Color(Quantity.color.r, Quantity.color.g, Quantity.color.b, 0);
        Bar.color = new Color(Bar.color.r, Bar.color.g, Bar.color.b, 0);
        BarBackground.color = new Color(BarBackground.color.r, BarBackground.color.g, BarBackground.color.b, 0);
        _myItem = null;
    }
}
